﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Indigo.Server.Web.Controllers
{
    public class PingController : Controller
    {
        [HttpGet("-/ping")]
        public async Task<IActionResult> Get()
        {
            return Ok();
        }
    }
}
