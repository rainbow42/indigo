﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Indigo.Server.Interfaces.Exceptions;
using Indigo.Server.Interfaces.Handlers;
using Indigo.Server.Interfaces.Utils;
using Indigo.Server.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;

namespace Indigo.Server.Web.Controllers
{
    [Route("/")]
    public class PackageController : Controller
    {
        private readonly IPackageMetadataHandler _metaDataHandler;
        private readonly IPackageTarballHander _tarballHander;
        private readonly IPublishPackageHandler _publishPackageHandler;
        private readonly IUnpublishPackageHandler _unpublishPackageHandler;
        private readonly IPackageMetadataParser _packageMetadataParser;
        private readonly IPackageMetadataValidator _packageMetadataValidator;
        private readonly IConfiguration _configuration;

        public PackageController(IPackageMetadataHandler metaDataHandler, IPackageTarballHander tarballHander,
            IPublishPackageHandler publishPackageHandler, IUnpublishPackageHandler unpublishPackageHandler,
            IPackageMetadataParser packageMetadataParser, IPackageMetadataValidator packageMetadataValidator,
            IConfiguration configuration)
        {
            _metaDataHandler = metaDataHandler;
            _tarballHander = tarballHander;
            _publishPackageHandler = publishPackageHandler;
            _unpublishPackageHandler = unpublishPackageHandler;
            _packageMetadataParser = packageMetadataParser;
            _packageMetadataValidator = packageMetadataValidator;
            _configuration = configuration;

        }

        [HttpGet("{package}")]
        public async Task<IActionResult> Get(string package, CancellationToken cancellationToken = default)
        {
            try
            {
                return Ok(JsonConvert.DeserializeObject(await _metaDataHandler.GetPackageMetadata(package, null, cancellationToken)));
            }
            catch (PackageNotFoundException)
            {
                return NotFound("Package not found");
            }
        }

        [HttpGet("{package}/{version}")]
        public async Task<IActionResult> Get(string package, string version, CancellationToken cancellationToken = default)
        {
            try
            {
                return Ok(JsonConvert.DeserializeObject(await _metaDataHandler.GetPackageMetadata(package, version, cancellationToken)));
            }
            catch (PackageNotFoundException)
            {
                return NotFound("Package not found");
            }
        }

        [HttpGet("{package}/-/{fileName}")]
        public async Task<IActionResult> GetTarBall(string package, string fileName, CancellationToken cancellationToken = default)
        {
            try
            {
                var data = await _tarballHander.GetTarBall(package, fileName, cancellationToken);
                var stream = new MemoryStream(data);
                return File(stream, "application/gzip", fileName);
            }
            catch (PackageNotFoundException)
            {
                return NotFound("Tarball not found");
            }
        }

        // To support npm versions below v0.3, these two methods have to be implemented
        // [HttpPut("{package}/-/{fileName}/{*extras}")]
        // [HttpPut("{package}/{version}/-tag/{tagName}")]
        [HttpPut("{package}/{_rev?}/{revision?}")]
        public async Task<IActionResult> Publish(string package, string _rev, string revision, [FromBody] PackageMetadata metadata, CancellationToken cancellationToken = default)
        {
            try
            {
                (var packageName, var packageVersion, var tarball, var tarballFileName) = _packageMetadataParser.ParseMetadata(metadata);

                _packageMetadataValidator.Validate(package, (packageName, packageVersion, tarball));

                await _publishPackageHandler.PublishPackage(package, packageVersion, metadata, tarball, tarballFileName, revision);

                return StatusCode(201, new { ok = "Package successfully published", success = true });
            }
            catch (InvalidPackageMetadataException ex)
            {
                return StatusCode(422, new { reason = ex.Message ?? "Invalid package metadata" });
            }
            catch (PackageAlreadyExistsException ex)
            {
                return StatusCode(409, new { reason = ex.Message ?? "Package already exists" });
            }
        }

        [HttpDelete("{package}/-rev/{revision}")]
        public async Task<IActionResult> UnpublishEntirePackage(string package, string revision, CancellationToken cancellationToken = default)
        {
            try
            {
                await _unpublishPackageHandler.UnpublishPackage(package, revision);
                return Ok();
            }
            catch (PackageNotFoundException)
            {
                return NotFound("Package not found");
            }
            catch (InvalidPackageMetadataException ex)
            {
                return StatusCode(422, new { reason = ex.Message ?? "Invalid request" });
            }
            catch (InvalidOperationException ex)
            {
                return StatusCode(422, new { reason = ex.Message ?? "Invalid operation" });
            }
        }
        [HttpDelete("{package}/-/{fileName}/-rev/{revision}")]
        public async Task<IActionResult> UnpublishPackage(string package, string fileName, string revision, CancellationToken cancellationToken = default)
        {
            try
            {
                await _unpublishPackageHandler.UnpublishPackage(package, fileName, revision);
                return Ok();
            }
            catch (PackageNotFoundException)
            {
                return NotFound("Tarball not found");
            }
            catch (InvalidPackageMetadataException ex)
            {
                return StatusCode(422, new { reason = ex.Message ?? "Invalid request" });
            }
            catch (InvalidOperationException ex)
            {
                return StatusCode(422, new { reason = ex.Message ?? "Invalid operation" });
            }
        }
    }
}