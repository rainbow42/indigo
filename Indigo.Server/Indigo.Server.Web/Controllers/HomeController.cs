﻿using Microsoft.AspNetCore.Mvc;

namespace Indigo.Server.Web.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return Ok("You got here");
        }
    }
}
