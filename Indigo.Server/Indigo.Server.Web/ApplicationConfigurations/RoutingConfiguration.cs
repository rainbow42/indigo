﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;

namespace Indigo.Server.Web.ApplicationConfigurations
{
    public static class RoutingConfiguration
    {
        public static IApplicationBuilder UseMvcWithApplicationRoutes(this IApplicationBuilder app)
        {
            app.UseMvc(ConfigureRoutes);
            return app;
        }

        private static void ConfigureRoutes(IRouteBuilder router)
        {
            
            router.MapRoute("default", "{controller}/{action}");
            //router.MapRoute("npm", "{package?}/{version?}", defaults: new { controller = "Package", action = "GetPackageMetadata" });
            //router.MapRoute("npm-withVersion", "{package}/{version?}", defaults: new { controller = "Package", action= "GetPackageMetadataForVersion" });
            router.MapRoute("tarball", "{package?}/-/{version}.tgz", defaults: new { controller = "Home", action = "GetTarball" });
        }
    }
}