using Indigo.Server.Web.Formatters;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using StructureMap.Graph;

namespace Indigo.Server.Web.ApplicationConfigurations
{
    public static class StructuremapConfigurationEx
    {
        public static void ConfigureThisApplicationWithDefaults(this IContainer container, IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                options.InputFormatters.Insert(0, new MetadataFormatter());
            });
            container.Configure(c => WithConfiguration(c, services));
            //container.AssertConfigurationIsValid(); // throws exception if default configuration is not present for an interface. 
        }

        private static void WithConfiguration(ConfigurationExpression config, IServiceCollection services)
        {
            config.Scan(WithScanningConfig);
            config.Populate(services);
        }

        private static void WithScanningConfig(IAssemblyScanner scanner)
        {
            scanner.AssembliesFromApplicationBaseDirectory(a => a.FullName.Contains("Indigo"));
            scanner.SingleImplementationsOfInterface();
            scanner.LookForRegistries();
            scanner.WithDefaultConventions();
        }
    }
}