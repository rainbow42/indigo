﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Indigo.Server.Web.ApplicationConfigurations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using StructureMap;

namespace Indigo.Server.Web
{
    public class Startup
    {
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvcCore();
            IContainer container = new Container();
            container.ConfigureThisApplicationWithDefaults(services);
            return container.GetInstance<IServiceProvider>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IConfiguration config)
        {
            if (!Directory.Exists("~/packages"))
                Directory.CreateDirectory("~/packages");
            loggerFactory.AddConsole();
            app.UseDeveloperExceptionPage();
            app.UseMvcWithApplicationRoutes();

        }
    }
}
