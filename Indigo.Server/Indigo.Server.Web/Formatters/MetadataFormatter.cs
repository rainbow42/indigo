﻿using Indigo.Server.Models;
using Indigo.Server.Utilities.Extensions;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.Server.Web.Formatters
{
    public class MetadataFormatter : TextInputFormatter
    {
        public MetadataFormatter()
        {
            SupportedEncodings.Add(Encoding.UTF8);
            SupportedEncodings.Add(Encoding.Unicode);
            SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/json"));
        }
        public override Task<InputFormatterResult> ReadRequestBodyAsync(InputFormatterContext context, Encoding encoding)
        {
            var request = context.HttpContext.Request;

            using (var streamReader = context.ReaderFactory(request.Body, encoding))
            {
                var type = context.ModelType;

                try
                {
                    var model = streamReader.ReadToEnd().ParseToPackageMetadata();
                    return InputFormatterResult.SuccessAsync(model);
                }
                catch (Exception ex)
                {
                    return InputFormatterResult.FailureAsync();
                }
            }
        }

        public override bool CanRead(InputFormatterContext context)
        {
            return context.ModelType == typeof(PackageMetadata);
        }

    }
}
