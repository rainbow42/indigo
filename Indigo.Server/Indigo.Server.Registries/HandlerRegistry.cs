﻿using Indigo.Server.Core.Handlers;
using Indigo.Server.Interfaces.Handlers;
using Indigo.Server.Interfaces.Infrastructure;
using Indigo.Server.Models.Storage;
using Indigo.Server.Plugins.Storage;
using StructureMap;
using System;
using System.Threading.Tasks;

namespace Indigo.Server.Registries
{
    public class HandlerRegistry : Registry
    {
        public HandlerRegistry()
        {


            For<IPackageMetadataHandler>().Use<PackageMetadataHandler>()
                .Ctor<IPackageMetadataProvider>("fileSystemMetadataProvider").Is(_ => _.GetInstance<IPackageMetadataProvider>("FileSystem"))
                .Ctor<IPackageMetadataProvider>("remoteMetadataProvider").Is(_ => _.GetInstance<IPackageMetadataProvider>("Http"))
                .Ctor<IBlobStore<PackageMetadataFile>>("metadataStore")
                .Is(_ => CreateStoreFrom(_.GetInstance<IBlobStorageProvider>().CreateOrGetExistingStore<PackageMetadataFile>("Cache")))
                .Ctor<bool>("isGetPackageFromRemote").Is(true)
                .Ctor<string>("currentDomain").Is("localhost")
                .Ctor<string>("remoteDomain").Is("registry.npmjs.org");


            For<IPackageTarballHander>().Use<PackageTarballHander>()
                .Ctor<ITarBallProvider>("fileSystemTarballProvider").Is(_ => _.GetInstance<ITarBallProvider>("FileSystem"))
                .Ctor<ITarBallProvider>("remoteTarballProvider").Is(_ => _.GetInstance<ITarBallProvider>("Http"))
                .Ctor<IBlobStore<PackageTarballFile>>("tarballStore")
                .Is(_ => CreateStoreFrom(_.GetInstance<IBlobStorageProvider>().CreateOrGetExistingStore<PackageTarballFile>("Cache")))
                .Ctor<bool>("isGetPackageFromRemote").Is(true);

            For<IPublishPackageHandler>().Use<PublishPackageHandler>()
                .Ctor<IBlobStore<PackageMetadataFile>>("metadataStore")
                .Is(_ => CreateStoreFrom(_.GetInstance<IBlobStorageProvider>().CreateOrGetExistingStore<PackageMetadataFile>("Private")))
                .Ctor<IBlobStore<PackageTarballFile>>("tarballStore")
                .Is(_ => CreateStoreFrom(_.GetInstance<IBlobStorageProvider>().CreateOrGetExistingStore<PackageTarballFile>("Private")));

            For<IUnpublishPackageHandler>().Use<UnpublishPackageHandler>().Ctor<IBlobStore<PackageMetadataFile>>("metadataStore")
                .Is(_ => CreateStoreFrom(_.GetInstance<IBlobStorageProvider>().CreateOrGetExistingStore<PackageMetadataFile>("Private")))
                .Ctor<IBlobStore<PackageTarballFile>>("tarballStore")
                .Is(_ => CreateStoreFrom(_.GetInstance<IBlobStorageProvider>().CreateOrGetExistingStore<PackageTarballFile>("Private")));

        }

        private T CreateStoreFrom<T>(Task<T> creatorTask)
        {
            creatorTask.Wait();
            return creatorTask.Result;
        }
    }
}