﻿using System;
using System.Net.Http;
using StructureMap;

namespace Indigo.Server.Registries
{
    public class UtilityRegistry : Registry
    {
        public UtilityRegistry()
        {
            For<HttpClient>().Use(new HttpClient() { BaseAddress = new Uri("http://registry.npmjs.com") }).Named("npmRegistry");
        }
    }
}