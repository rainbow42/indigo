﻿using System.Net.Http;
using System.Threading.Tasks;
using Indigo.Server.Infrastructure;
using Indigo.Server.Infrastructure.PackageMetadataProviders;
using Indigo.Server.Infrastructure.TarBallProviders;
using Indigo.Server.Infrastructure.Utils;
using Indigo.Server.Interfaces.Infrastructure;
using Indigo.Server.Interfaces.Utils;
using Indigo.Server.Models.Storage;
using Indigo.Server.Plugins.Storage;
using Microsoft.Extensions.Logging;
using StructureMap;

namespace Indigo.Server.Registries
{
    public class ProviderRegistry : Registry
    {
        public ProviderRegistry()
        {
            For<IPackageMetadataProvider>().Use<FileSystemPackageMetadataProvider>()
                .Ctor<IBlobStore<PackageMetadataFile>>("privateMetadataStore")
                .Is(_ => CreateStoreFrom(_.GetInstance<IBlobStorageProvider>().CreateOrGetExistingStore<PackageMetadataFile>("Private")))
                .Ctor<IBlobStore<PackageMetadataFile>>("cachedMetadataStore")
                .Is(_ => CreateStoreFrom(_.GetInstance<IBlobStorageProvider>().CreateOrGetExistingStore<PackageMetadataFile>("Cache")))
                .Named("FileSystem");

            For<IPackageMetadataProvider>().Add<HttpPackageMetadataProvider>()
                .Ctor<HttpClient>().Is(_ => _.GetInstance<HttpClient>("npmRegistry"))
                .Named("Http");

            For<ITarBallProvider>().Use<FileSystemTarBallProvider>()
                .Ctor<IBlobStore<PackageTarballFile>>("privateTarballStore")
                .Is(_ => CreateStoreFrom(_.GetInstance<IBlobStorageProvider>().CreateOrGetExistingStore<PackageTarballFile>("Private")))
                .Ctor<IBlobStore<PackageTarballFile>>("cachedTarballStore")
                .Is(_ => CreateStoreFrom(_.GetInstance<IBlobStorageProvider>().CreateOrGetExistingStore<PackageTarballFile>("Cache")))
                .Named("FileSystem");

            For<ITarBallProvider>().Use<HttpTarBallProvider>()
                .Ctor<HttpClient> ("httpClient").Is(_ => _.GetInstance<HttpClient>("npmRegistry")).Named("Http");

            For<IPackageSaver>().Use<FileSystemPackageSaver>()
                .Ctor<IFileWrapper>().Is<FileWrapper>()
                .Ctor<string>("rootPath").Is("~/Packages");


            For<ILogger>().Use<Logger>();
        }


        private T CreateStoreFrom<T>(Task<T> creatorTask)
        {
            creatorTask.Wait();
            return creatorTask.Result;
        }
    }
}