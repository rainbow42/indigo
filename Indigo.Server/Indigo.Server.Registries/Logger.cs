﻿using Microsoft.Extensions.Logging;
using System;
using System.IO;

namespace Indigo.Server.Registries
{
    public class Logger : ILogger, IDisposable
    {
        public IDisposable BeginScope<TState>(TState state)
        {
            return this;
        }

        public void Dispose()
        {
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            var message = formatter(state, exception);
            File.AppendAllText(@"C:\Users\SarathKCM\Desktop\log.log", $"\r\n [info] {message}");
        }
    }
}
