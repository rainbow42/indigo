﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Indigo.Server.Models
{
    public class PackageMetadataAttachment : JObject
    {
        public PackageMetadataAttachment() : base() { }
        public PackageMetadataAttachment(JObject other) : base(other) { }
        public PackageMetadataAttachment(params object[] content) : base(content) { }
        public PackageMetadataAttachment(object content) : base(content) { }

        public static new PackageMetadataAttachment Parse(string json)
        {
            return new PackageMetadataAttachment(JObject.Parse(json));
        }

        [JsonIgnore]
        public string ContentType
        {
            get
            {
                return (string)this["content_type"];
            }
        }

        [JsonIgnore]
        public string Data
        {
            get
            {
                return (string)this["data"];
            }
        }

        [JsonIgnore]
        public Int64 Length
        {
            get
            {
                return (Int64)this["length"];
            }
        }

        [JsonIgnore]
        public string ShaSum
        {
            get
            {
                return (string)this["shasum"];
            }
        }

        [JsonIgnore]
        public string Version
        {
            get
            {
                return (string)this["version"];
            }
        }

    }
}
