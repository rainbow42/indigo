﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Indigo.Server.Models
{
    public class PackageVersionMetadata:JObject
    {
        public PackageVersionMetadata() : base() { }
        public PackageVersionMetadata(JObject other) : base(other) { }
        public PackageVersionMetadata(params object[] content) : base(content) { }
        public PackageVersionMetadata(object content) : base(content) { }

        public static new PackageVersionMetadata Parse(string json)
        {
            return new PackageVersionMetadata(JObject.Parse(json));
        }

        [JsonIgnore]
        public dynamic TarballFilename
        {
            get
            {
                return ((string)this["dist"]["tarball"]).Split("/").Last();
            }
        }

        [JsonIgnore]
        public dynamic ShaSum
        {
            get
            {
                return (string)this["dist"]["shasum"];
            }
        }

        [JsonIgnore]
        public string Version
        {
            get
            {
                return (string)this["version"];
            }
        }

        [JsonIgnore]
        public string Name
        {
            get
            {
                return (string)this["name"];
            }
        }
    }
}
