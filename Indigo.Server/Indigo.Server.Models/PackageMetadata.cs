﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Indigo.Server.Models
{
    public class PackageMetadata : JObject
    {
        public PackageMetadata() : base() { }
        public PackageMetadata(JObject other) : base(other) { }
        public PackageMetadata(params object[] content) : base(content) { }
        public PackageMetadata(object content) : base(content) { }


        public static new PackageMetadata Parse(string json)
        {
            return new PackageMetadata(JObject.Parse(json));
        }

        [JsonIgnore]
        public string Name
        {
            get
            {
                return (string)this["name"]; ;
            }
        }

        [JsonIgnore]
        public Dictionary<string, PackageMetadataAttachment> Attachments
        {
            get
            {
                return this["_attachments"]?.ToObject<Dictionary<string, JObject>>()?.ToDictionary(kv => kv.Key, kv => new PackageMetadataAttachment(kv.Value));
            }
        }

        public void AddOrUpdateAttachment(string fileName, PackageMetadataAttachment attachment)
        {
            this["_attachments"] = this["_attachments"] ?? new JObject();
            this["_attachments"][fileName] = attachment;
        }

        [JsonIgnore]
        public Dictionary<string, PackageVersionMetadata> Versions
        {
            get
            {
                return this["versions"]?.ToObject<Dictionary<string, JObject>>()?.ToDictionary(kv => kv.Key, kv => new PackageVersionMetadata(kv.Value));
            }
        }

        public void AddOrUpdateVersion(PackageVersionMetadata version)
        {
            this["versions"] = this["versions"] ?? new JObject();
            this["versions"][version.Version] = version;
        }

        [JsonIgnore]
        public Dictionary<string, string> DistTags
        {
            get
            {
                return this["dist-tags"].ToObject<Dictionary<string, string>>();
            }
        }

        public void AddOrUpdateDistTag(string name, string value)
        {
            this["dist-tags"] = this["dist-tags"] ?? new JObject();
            this["dist-tags"][name] = value;
        }

        [JsonIgnore]
        public string Revision
        {
            get
            {
                return (string)this["_rev"];
            }
        }


    }
}
