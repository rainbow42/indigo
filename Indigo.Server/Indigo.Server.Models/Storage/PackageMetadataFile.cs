﻿using Indigo.Server.Plugins.Storage.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Indigo.Server.Models.Storage
{
    public class PackageMetadataFile : IBlobMetadata
    {
        public string CollectionName { get; set; }
        public string FileName { get; set; }
    }
}
