﻿using Indigo.Server.Interfaces.Exceptions;
using Indigo.Server.Interfaces.Utils;
using Indigo.Server.Models;
using Indigo.Server.Utilities.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Indigo.Server.Core.Utls
{
    public class PackageMetadataParser: IPackageMetadataParser
    {
        public (string packageName, string packageVersion, byte[] tarball, string tarballFileName) ParseMetadata(PackageMetadata metadata)
        {
            try
            {
                var tarballAttachmentFromMetadata = metadata.Attachments?.Values.First();
                var tarball = tarballAttachmentFromMetadata.IsNotNull() ? Convert.FromBase64String(tarballAttachmentFromMetadata.Data) : null;
                var tarballFileName = metadata.Attachments?.Keys.First();

                var packageVersion = (string)metadata.Versions.Values.First().Version;
                var packageName = (string)metadata.Versions.Values.First().Name;

                return (packageName, packageVersion, tarball, tarballFileName);
            }
            catch (Exception ex)
            {
                throw new InvalidPackageMetadataException(ex.Message);
            }
        }
    }
}
