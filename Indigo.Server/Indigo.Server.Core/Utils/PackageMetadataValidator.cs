﻿using Indigo.Server.Interfaces.Exceptions;
using Indigo.Server.Interfaces.Utils;
using Indigo.Server.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Indigo.Server.Core.Utils
{
    public class PackageMetadataValidator : IPackageMetadataValidator
    {
        public void Validate(string packageName, (string packageName, string packageVersion, byte[] tarball) parsedPackageDetails)
        {
            if (!packageName.EqualsIgnoreCase(parsedPackageDetails.packageName))
                throw new InvalidPackageMetadataException("Package name does not match with the metadata");
        }
    }
}
