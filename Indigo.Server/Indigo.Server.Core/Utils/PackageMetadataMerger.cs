﻿using Indigo.Server.Interfaces.Utils;
using Indigo.Server.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Indigo.Server.Core.Utils
{
    public class PackageMetadataMerger : IPackageMetadataMerger
    {
        public PackageMetadata MergePackageMetadata(PackageMetadata current, PackageMetadata newVersion)
        {
            var currentMetadata = new PackageMetadata(current ?? newVersion);
            PackageVersionMetadata newPackageVersion = newVersion.Versions.First().Value;

            currentMetadata.AddOrUpdateVersion(newPackageVersion);

            foreach (var distTag in newVersion.DistTags)
            {
                currentMetadata.AddOrUpdateDistTag(distTag.Key, distTag.Value);
            }

            var attachmentObject = PackageMetadataAttachment.Parse(JsonConvert.SerializeObject(new { version = newPackageVersion.Version, shasum = newPackageVersion.ShaSum }));
            currentMetadata.AddOrUpdateAttachment(newPackageVersion.TarballFilename, attachmentObject);

            return currentMetadata;
        }
    }
}
