﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Indigo.Server.Interfaces.Exceptions;
using Indigo.Server.Interfaces.Handlers;
using Indigo.Server.Interfaces.Infrastructure;
using Indigo.Server.Models.Storage;
using Indigo.Server.Plugins.Storage;

namespace Indigo.Server.Core.Handlers
{
    public class PackageMetadataHandler : IPackageMetadataHandler
    {
        private readonly IPackageMetadataProvider _fileSystemMetadataProvider;
        private readonly IPackageMetadataProvider _remoteMetadataProvider;
        private readonly IBlobStore<PackageMetadataFile> _metadataStore;
        private readonly bool _isGetPackageFromRemote;
        private readonly string _currentDomain;
        private readonly string _remoteDomain;

        public PackageMetadataHandler(IPackageMetadataProvider fileSystemMetadataProvider, 
            IPackageMetadataProvider remoteMetadataProvider, IBlobStore<PackageMetadataFile> metadataStore, 
            bool isGetPackageFromRemote, string currentDomain, string remoteDomain)
        {
            _fileSystemMetadataProvider = fileSystemMetadataProvider;
            _remoteMetadataProvider = remoteMetadataProvider;
            _metadataStore = metadataStore;
            _isGetPackageFromRemote = isGetPackageFromRemote;
            _currentDomain = currentDomain;
            _remoteDomain = remoteDomain;
        }
        public async Task<string> GetPackageMetadata(string package, string version, CancellationToken cancellationToken =  default)
        {
            try
            {
                var responseFromFileSystem = await _fileSystemMetadataProvider.GetPackageMetadata(package, version);
                if (responseFromFileSystem == null && _isGetPackageFromRemote)
                {
                    var responseFromHttp = await _remoteMetadataProvider.GetPackageMetadata(package, version);
                    var fileName = $"{package}/package.json";
                    var responseToSave = responseFromHttp.Replace(_remoteDomain, _currentDomain);
                    await _metadataStore.SaveFile(fileName, responseToSave, new PackageMetadataFile { CollectionName = _metadataStore .CollectionName, FileName = fileName});
                    return responseToSave;
                }
                if(responseFromFileSystem == null)
                    throw new PackageNotFoundException("Package not found");
                return responseFromFileSystem;
            }
            catch (HttpRequestException e) when(e.Message.Contains("404 (Not Found)"))
            {
                throw new PackageNotFoundException("Package not found");
            }
           
        }
    }
}