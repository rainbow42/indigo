﻿using Indigo.Server.Interfaces.Exceptions;
using Indigo.Server.Interfaces.Handlers;
using Indigo.Server.Interfaces.Infrastructure;
using Indigo.Server.Models;
using Indigo.Server.Models.Storage;
using Indigo.Server.Plugins.Storage;
using Indigo.Server.Utilities.Extensions;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.Server.Core.Handlers
{
    public class UnpublishPackageHandler : IUnpublishPackageHandler
    {
        private readonly IBlobStore<PackageMetadataFile> _metadataStore;
        private readonly IBlobStore<PackageTarballFile> _tarballStore;

        public UnpublishPackageHandler(IBlobStore<PackageMetadataFile> metadataStore,
            IBlobStore<PackageTarballFile> tarballStore)
        {
            _metadataStore = metadataStore;
            _tarballStore = tarballStore;
        }

        public async Task UnpublishPackage(string packageName, string revision)
        {
            var currentMetaData = await GetCurrentPackageMetadata(packageName);
            var tarballs = currentMetaData.Versions.Values.Select(pkg => pkg.TarballFilename).ToList();
            tarballs.ForEach(async tarball =>
            {
                await _tarballStore.DeleteFile($"{packageName}/{tarball}");
            });
            await _metadataStore.DeleteFile($"{packageName}/package.json");

        }

        public async Task UnpublishPackage(string packageName, string fileName, string revision)
        {
            await _tarballStore.DeleteFile($"{packageName}/{fileName}");
        }


        private async Task<PackageMetadata> GetCurrentPackageMetadata(string packageName)
        {
            var currentMetadataStringFromStorage = await _metadataStore.GetFile($"{packageName}/package.json");
            return Encoding.UTF8.GetString(currentMetadataStringFromStorage).ParseToPackageMetadata();
        }

        #region Unused methods - keeping them for refering later
        private async Task ValidateUnpublishRequest(string packageName, string fileName, string revision)
        {
            var packageAlreadyExists = await DoesPackageAlreadyExist(packageName);
            if (!packageAlreadyExists)
                throw new InvalidOperationException("Cannot delete non-existing package");
            if (revision.IsNull())
                throw new InvalidPackageMetadataException("Cannot delete tarball without the latest revision tag.");

            var packageMetadataFromStorage = await GetCurrentPackageMetadata(packageName);

            if (!packageMetadataFromStorage.Revision.EqualsIgnoreCase(revision))
                throw new InvalidPackageMetadataException("The revision tag provided does not match the latest revision tag.");
        }

        private async Task<bool> DoesPackageAlreadyExist(string packageName)
        {
            return await _metadataStore.IsFileExists($"{packageName}/package.json");
        }
        #endregion

    }
}
