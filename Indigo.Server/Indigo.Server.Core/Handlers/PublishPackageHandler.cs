﻿using Indigo.Server.Interfaces.Exceptions;
using Indigo.Server.Interfaces.Handlers;
using Indigo.Server.Interfaces.Infrastructure;
using Indigo.Server.Interfaces.Utils;
using Indigo.Server.Models;
using Indigo.Server.Models.Storage;
using Indigo.Server.Plugins.Storage;
using Indigo.Server.Utilities.Extensions;
using System;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.Server.Core.Handlers
{
    public class PublishPackageHandler : IPublishPackageHandler
    {
        private readonly IBlobStore<PackageMetadataFile> _metadataStore;
        private readonly IBlobStore<PackageTarballFile> _tarballStore;
        private readonly IPackageMetadataMerger _packageMetadataMerger;

        public PublishPackageHandler(IBlobStore<PackageMetadataFile> metadataStore,
            IBlobStore<PackageTarballFile> tarballStore, IPackageMetadataMerger packageMetadataMerger)
        {
            _metadataStore = metadataStore;
            _tarballStore = tarballStore;
            _packageMetadataMerger = packageMetadataMerger;
        }
        public async Task PublishPackage(string packageName, string packageVersion, PackageMetadata newMetadata, byte[] fileContent, string tarballFileName, string revision = null)
        {
            var currentMetadata = await GetCurrentPackageMetadata(packageName);

            ValidateIncomingPackage(packageName, packageVersion, newMetadata, currentMetadata, revision);

            var modifiedMetaData = revision.IsNull() ? _packageMetadataMerger.MergePackageMetadata(currentMetadata, newMetadata) : newMetadata;
            await _metadataStore.SaveFile($"{packageName}/package.json", modifiedMetaData.ToString(), new PackageMetadataFile { CollectionName = _metadataStore .CollectionName, FileName = $"{packageName}/package.json" });

            if (fileContent.IsNotNull())
                await _tarballStore.SaveFile($"{packageName}/{tarballFileName}", fileContent, new PackageTarballFile { CollectionName = _tarballStore.CollectionName, FileName = $"{packageName}/{tarballFileName}" });
        }

        private void ValidateIncomingPackage(string packageName, string packageVersion, PackageMetadata newMetaData, PackageMetadata currentMetaData, string revision)
        {
            if (currentMetaData != null && !revision.IsNull() && !currentMetaData.Revision.EqualsIgnoreCase(revision))
                throw new InvalidPackageMetadataException("Document update conflict.");

            var packageVersionAlreadyExists = currentMetaData?.Versions.ContainsKey(packageVersion);
            if (packageVersionAlreadyExists == true && revision.IsNull())
                throw new PackageAlreadyExistsException("Cannot modify existing package version.");
        }

        private async Task<PackageMetadata> GetCurrentPackageMetadata(string packageName)
        {
            if (await _metadataStore.IsFileExists($"{packageName}/package.json"))
            {
                var currentMetadataStringFromStorage = await _metadataStore.GetFile($"{packageName}/package.json");
                return Encoding.UTF8.GetString(currentMetadataStringFromStorage).ParseToPackageMetadata();
            }
            return null;
        }
    }
}
