﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Indigo.Server.Interfaces.Exceptions;
using Indigo.Server.Interfaces.Handlers;
using Indigo.Server.Interfaces.Infrastructure;
using Indigo.Server.Interfaces.Utils;
using Indigo.Server.Models.Storage;
using Indigo.Server.Plugins.Storage;

namespace Indigo.Server.Core.Handlers
{
    public class PackageTarballHander : IPackageTarballHander
    {
        private readonly ITarBallProvider _fileSystemTarballProvider;
        private readonly ITarBallProvider _remoteTarballProvider;
        private readonly IBlobStore<PackageTarballFile> _tarballStore;
        private readonly bool _isGetPackageFromRemote;

        public PackageTarballHander(ITarBallProvider fileSystemTarballProvider, ITarBallProvider remoteTarballProvider, 
            IBlobStore<PackageTarballFile> tarballStore,
            bool isGetPackageFromRemote)
        {
            _fileSystemTarballProvider = fileSystemTarballProvider;
            _remoteTarballProvider = remoteTarballProvider;
            _tarballStore = tarballStore;
            _isGetPackageFromRemote = isGetPackageFromRemote;
        }

        public async Task<byte[]> GetTarBall(string packageName, string fileName, CancellationToken cancellationToken = default)
        {
            try
            {
                var package = await _fileSystemTarballProvider.GetGZipBytes(packageName, fileName);
                if (package == null && _isGetPackageFromRemote)
                {
                    package = await _remoteTarballProvider.GetGZipBytes(packageName, fileName);
                    var filePathInCache = $"{packageName}/{fileName}";
                    await _tarballStore.SaveFile(filePathInCache, package, new PackageTarballFile { CollectionName = _tarballStore.CollectionName, FileName = filePathInCache });
                    return package;
                }
                if(package == null)
                    throw new PackageNotFoundException("Package not found");
                return package;
            }
            catch (HttpRequestException e) when(e.Message.Contains("404 (Not Found)"))
            {
                throw new PackageNotFoundException("Package not found");
            }
        }
    }
}