﻿using System.Text;
using System.Threading.Tasks;
using Indigo.Server.Interfaces.Infrastructure;
using Indigo.Server.Interfaces.Utils;
using Indigo.Server.Models.Storage;
using Indigo.Server.Plugins.Storage;

namespace Indigo.Server.Infrastructure.PackageMetadataProviders
{
    public class FileSystemPackageMetadataProvider : IPackageMetadataProvider
    {
        private readonly IBlobStore<PackageMetadataFile> _privateMetadataStore;
        private readonly IBlobStore<PackageMetadataFile> _cachedMetadataStore;

        public FileSystemPackageMetadataProvider(IBlobStore<PackageMetadataFile> privateMetadataStore, IBlobStore<PackageMetadataFile> cachedMetadataStore)
        {
            _privateMetadataStore = privateMetadataStore;
            _cachedMetadataStore = cachedMetadataStore;
        }
        public async Task<string> GetPackageMetadata(string package, string version)
        {
            var fileName = $"{package}/package.json";
            if (await _privateMetadataStore.IsFileExists(fileName))
                return Encoding.UTF8.GetString(await _privateMetadataStore.GetFile(fileName));
            if (await _cachedMetadataStore.IsFileExists(fileName))
                return Encoding.UTF8.GetString(await _cachedMetadataStore.GetFile(fileName));
            return null;
        }
    }
}