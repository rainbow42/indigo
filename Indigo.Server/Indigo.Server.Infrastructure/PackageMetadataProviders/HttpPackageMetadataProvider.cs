﻿using System.Net.Http;
using System.Threading.Tasks;
using Indigo.Server.Interfaces.Infrastructure;

namespace Indigo.Server.Infrastructure.PackageMetadataProviders
{
    public class HttpPackageMetadataProvider : IPackageMetadataProvider
    {
        private readonly HttpClient _httpClient;

        public HttpPackageMetadataProvider(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        public Task<string> GetPackageMetadata(string package, string version)
        {
            var address = string.IsNullOrWhiteSpace(version) ? $"{package}" : $"{package}/{version}";
            return _httpClient.GetStringAsync(address);
        }
    }
}