﻿using System.Net.Http;
using System.Threading.Tasks;
using Indigo.Server.Interfaces.Infrastructure;

namespace Indigo.Server.Infrastructure.TarBallProviders
{
    public class HttpTarBallProvider : ITarBallProvider
    {
        private readonly HttpClient _httpClient;

        public HttpTarBallProvider(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<byte[]> GetGZipBytes(string packageName, string fileName)
        {
            var uri = $"{packageName}/-/{fileName}";
            var response = await _httpClient.GetAsync(uri);
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsByteArrayAsync();
            return content;
        }
    }
}