﻿using System.IO;
using System.Threading.Tasks;
using Indigo.Server.Interfaces.Infrastructure;
using Indigo.Server.Interfaces.Utils;
using Indigo.Server.Models.Storage;
using Indigo.Server.Plugins.Storage;

namespace Indigo.Server.Infrastructure.TarBallProviders
{
    public class FileSystemTarBallProvider : ITarBallProvider
    {
        private readonly IBlobStore<PackageTarballFile> _privateTarballStore;
        private readonly IBlobStore<PackageTarballFile> _cachedTarballStore;

        public FileSystemTarBallProvider(IBlobStore<PackageTarballFile> privateTarballStore, IBlobStore<PackageTarballFile> cachedTarballStore)
        {
            _privateTarballStore = privateTarballStore;
            _cachedTarballStore = cachedTarballStore;
        }

        public async Task<byte[]> GetGZipBytes(string packageName, string fileName)
        {
            var filePathInCache = $"{packageName}/{fileName}";
            if (await _privateTarballStore.IsFileExists(filePathInCache))
                return await _privateTarballStore.GetFile(filePathInCache);
            if (await _cachedTarballStore.IsFileExists(filePathInCache))
                return await _cachedTarballStore.GetFile(filePathInCache);
            return null;
        }
    }
}