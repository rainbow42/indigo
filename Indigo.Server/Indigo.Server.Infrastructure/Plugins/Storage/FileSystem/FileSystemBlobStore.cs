﻿using Indigo.Server.Interfaces.Utils;
using Indigo.Server.Plugins.Storage;
using Indigo.Server.Plugins.Storage.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Indigo.Server.Infrastructure.Plugins.Storage.FileSystem
{
    public class FileSystemBlobStore<T> : IBlobStore<T> where T : IBlobMetadata
    {
        public string CollectionName { get; private set; }


        private readonly string _directoryPath;


        private readonly IFileWrapper _fileWrapper;

        public FileSystemBlobStore(string collectionName, string directoryPath, IFileWrapper fileWrapper)
        {
            CollectionName = collectionName;
            _directoryPath = directoryPath;
            _fileWrapper = fileWrapper;
        }

        private string GetFilepath(string fileName)
        {
            return Path.Combine(_directoryPath, fileName);
        }

        private string GetMetadataFilePath(string fileName)
        {
            return $"{Path.Combine(_directoryPath, fileName)}.indigometadata";
        }
        public async Task DeleteFile(string fileName)
        {
            _fileWrapper.Delete(GetFilepath(fileName));
        }

        public async Task<byte[]> GetFile(string fileName)
        {
            return await _fileWrapper.ReadAllBytesAsync(GetFilepath(fileName));
        }

        public async Task<T> GetMetadata(string fileName)
        {
            var metadataText = await _fileWrapper.ReadAllTextAsync(GetMetadataFilePath(fileName));
            return JsonConvert.DeserializeObject<T>(metadataText);
        }

        public async Task<bool> IsFileExists(string fileName)
        {
            return _fileWrapper.Exists(GetFilepath(fileName));
        }

        public async Task SaveFile(string fileName, byte[] data, T metadata, bool overwrite = false)
        {
            await _fileWrapper.WriteAllBytesAsync(GetFilepath(fileName), data);
            await _fileWrapper.WriteAllTextAsync(GetMetadataFilePath(fileName), JsonConvert.SerializeObject(metadata));
        }

        public async Task SaveFile(string fileName, string data, T metadata, bool overwrite = false)
        {
            await _fileWrapper.WriteAllTextAsync(GetFilepath(fileName), data);
            await _fileWrapper.WriteAllTextAsync(GetMetadataFilePath(fileName), JsonConvert.SerializeObject(metadata));
        }


        public async Task UpdateMetadata(string fileName, T metadata)
        {
            await _fileWrapper.WriteAllTextAsync(GetMetadataFilePath(fileName), JsonConvert.SerializeObject(metadata));
        }

        public async Task UpdateFileContent(string fileName, byte[] data)
        {
            await _fileWrapper.WriteAllBytesAsync(GetFilepath(fileName), data);
        }

        public async Task UpdateFileContent(string fileName, string data)
        {
            await _fileWrapper.WriteAllTextAsync(GetFilepath(fileName), data);
        }

        public async Task<IEnumerable<T>> SearchFiles(Func<T, bool> criteriaForMetadata)
        {
            return GetMetadataMatchingCriteria(criteriaForMetadata);
        }

        private IEnumerable<T> GetMetadataMatchingCriteria(Func<T, bool> criteriaForMetadata)
        {
            var filesAvailable = Directory.GetFiles(_directoryPath).Where(file => file.EndsWith(".indigometadata"));
            foreach (var file in filesAvailable)
            {
                var metadata = JsonConvert.DeserializeObject<T>(_fileWrapper.ReadAllText(file));
                if (criteriaForMetadata(metadata))
                    yield return metadata;
            }
        }

        public async Task<IEnumerable<string>> SearchFiles(Func<string, bool> criteriaForFileName)
        {
            return GetFileNamesMatchingCriteria(criteriaForFileName);
        }

        private IEnumerable<string> GetFileNamesMatchingCriteria(Func<string, bool> criteriaForFileName)
        {
            var filesAvailable = Directory.GetFiles(_directoryPath).Where(file => !file.EndsWith(".indigometadata"));
            foreach (var file in filesAvailable)
            {
                if (criteriaForFileName(file))
                    yield return file;
            }
        }
    }
}
