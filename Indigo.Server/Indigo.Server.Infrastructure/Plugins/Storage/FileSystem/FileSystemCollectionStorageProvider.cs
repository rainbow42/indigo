﻿using Indigo.Server.Infrastructure.Utils;
using Indigo.Server.Plugins.Storage;
using Indigo.Server.Plugins.Storage.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.Server.Infrastructure.Plugins.Storage.FileSystem
{
    public class FileSystemCollectionStorageProvider : ICollectionStorageProvider
    {
        public async Task<ICollectionStore<T>> CreateOrGetExistingStore<T>(string collectionName) where T : ICollectionItem
        {
            var directoryInfo = new DirectoryWrapper().CreateOrGetDirectory($"~/Collections");
            var fileWrapper = new FileWrapper();
            if (!fileWrapper.Exists($"Storage/Collections/{collectionName}.json"))
            {
                await fileWrapper.WriteAllTextAsync($"Storage/Collections/{collectionName}.json", "[]");
            }
            return new FileSystemCollectionStore<T>(collectionName, new FileInfo($"Storage/Collections/{collectionName}.json").FullName, fileWrapper);
        }

        public async Task DeleteStore<T>(string collectionName) where T : ICollectionItem
        {
            new FileWrapper().Delete($"Storage/Collections/{collectionName}.json");
        }
    }
}
