﻿using Indigo.Server.Infrastructure.Utils;
using Indigo.Server.Plugins.Storage;
using Indigo.Server.Plugins.Storage.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.Server.Infrastructure.Plugins.Storage.FileSystem
{
    public class FileSystemBlobStorageProvider : IBlobStorageProvider
    {
        public async Task<IBlobStore<T>> CreateOrGetExistingStore<T>(string collectionName) where T : IBlobMetadata
        {
            var directoryInfo = new DirectoryWrapper().CreateOrGetDirectory($"~/Blob/{collectionName}");
            return new FileSystemBlobStore<T>(collectionName, directoryInfo.FullName, new FileWrapper());
        }

        public async Task DeleteStore<T>(string collectionName) where T : IBlobMetadata
        {
            new DirectoryWrapper().DeleteDirectory($"Storage/Blob/{collectionName}");
        }
    }
}
