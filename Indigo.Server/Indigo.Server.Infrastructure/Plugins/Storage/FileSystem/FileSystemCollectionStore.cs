﻿using Indigo.Server.Interfaces.Utils;
using Indigo.Server.Plugins.Storage;
using Indigo.Server.Plugins.Storage.Models;
using Indigo.Server.Utilities.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Indigo.Server.Infrastructure.Plugins.Storage.FileSystem
{
    public class FileSystemCollectionStore<T> : ICollectionStore<T> where T : ICollectionItem
    {
        public string CollectionName { get; private set; }


        private readonly string _filePath;
        private string _tempFilePath
        {
            get
            {
                return $"{_filePath}.temp";
            }
        }

        private readonly IFileWrapper _fileWrapper;
        static SemaphoreSlim _semaphore = new SemaphoreSlim(1, 1);

        public FileSystemCollectionStore(string collectionName, string filePath, IFileWrapper fileWrapper)
        {
            CollectionName = collectionName;
            _filePath = filePath;
            _fileWrapper = fileWrapper;
        }

        private IEnumerable<T> GetCollection()
        {
            var jsonSerializer = JsonSerializer.Create();
            using (FileStream fileStream = new FileStream(_filePath, FileMode.Open, FileAccess.Read))
            using (StreamReader fileReader = new StreamReader(fileStream))
            using (JsonTextReader jsonReader = new JsonTextReader(fileReader))
            {
                while (jsonReader.Read())
                {
                    if (jsonReader.TokenType == JsonToken.StartObject)
                    {
                        yield return jsonSerializer.Deserialize<T>(jsonReader);
                    }
                }
            }
        }

        private void ValidateCollectionItem(T item)
        {
            if (item.ID.IsNullOrWhiteSpace())
                throw new InvalidDataException("ID cannot be empty");
        }

        public async Task Delete(T item)
        {
            ValidateCollectionItem(item);
            await _semaphore.WaitAsync();
            try
            {
                var listOfCollectionItems = GetCollection();
                using (FileStream fileStream = new FileStream(_tempFilePath, FileMode.Open, FileAccess.Write))
                using (StreamWriter fileWriter = new StreamWriter(fileStream))
                using (JsonTextWriter jsonWriter = new JsonTextWriter(fileWriter))
                {
                    jsonWriter.WriteStartArray();
                    foreach (var collectionItem in listOfCollectionItems)
                    {
                        if (!collectionItem.ID.EqualsIgnoreCase(item.ID))
                            await jsonWriter.WriteValueAsync(collectionItem);
                    }
                    jsonWriter.WriteEndArray();
                }
                _fileWrapper.Delete(_filePath);
                _fileWrapper.Move(_tempFilePath, _filePath);
            }
            finally
            {
                _semaphore.Release();
            }
        }

        public async Task Delete(string id)
        {
            await _semaphore.WaitAsync();
            try
            {
                var listOfCollectionItems = GetCollection();
                using (FileStream fileStream = new FileStream(_tempFilePath, FileMode.Open, FileAccess.Write))
                using (StreamWriter fileWriter = new StreamWriter(fileStream))
                using (JsonTextWriter jsonWriter = new JsonTextWriter(fileWriter))
                {
                    jsonWriter.WriteStartArray();
                    foreach (var collectionItem in listOfCollectionItems)
                    {
                        if (!collectionItem.ID.EqualsIgnoreCase(id))
                            await jsonWriter.WriteValueAsync(collectionItem);
                    }
                    jsonWriter.WriteEndArray();
                }
                _fileWrapper.Delete(_filePath);
                _fileWrapper.Move(_tempFilePath, _filePath);
            }
            finally
            {
                _semaphore.Release();
            }
        }

        public async Task<IEnumerable<T>> Get(Func<T, bool> criteria)
        {
            await _semaphore.WaitAsync();
            try
            {
                return GetCollection().Where(criteria);
            }
            finally
            {
                _semaphore.Release();
            }
        }

        public async Task<T> GetOne(string id)
        {
            await _semaphore.WaitAsync();
            try
            {
                return GetCollection().FirstOrDefault(item => item.ID.EqualsIgnoreCase(id));
            }
            finally
            {
                _semaphore.Release();
            }
        }

        public async Task<T> GetOne(Func<T, bool> criteria)
        {
            await _semaphore.WaitAsync();
            try
            {
                return GetCollection().FirstOrDefault(criteria);
            }
            finally
            {
                _semaphore.Release();
            }
        }

        public async Task Insert(T item)
        {
            ValidateCollectionItem(item);
            await _semaphore.WaitAsync();
            try
            {
                var listOfCollectionItems = GetCollection();
                using (FileStream fileStream = new FileStream(_tempFilePath, FileMode.Open, FileAccess.Write))
                using (StreamWriter fileWriter = new StreamWriter(fileStream))
                using (JsonTextWriter jsonWriter = new JsonTextWriter(fileWriter))
                {
                    jsonWriter.WriteStartArray();
                    foreach (var collectionItem in listOfCollectionItems)
                    {
                        await jsonWriter.WriteValueAsync(collectionItem);
                    }
                    await jsonWriter.WriteValueAsync(item);
                    jsonWriter.WriteEndArray();
                }
                _fileWrapper.Delete(_filePath);
                _fileWrapper.Move(_tempFilePath, _filePath);
            }
            finally
            {
                _semaphore.Release();
            }
        }

        public async Task Update(T item)
        {
            ValidateCollectionItem(item);
            await _semaphore.WaitAsync();
            try
            {
                var listOfCollectionItems = GetCollection();
                var itemFromCollection = listOfCollectionItems.First(ci => ci.ID.EqualsIgnoreCase(item.ID)); // Todo: throw custom exception

                using (FileStream fileStream = new FileStream(_tempFilePath, FileMode.Open, FileAccess.Write))
                using (StreamWriter fileWriter = new StreamWriter(fileStream))
                using (JsonTextWriter jsonWriter = new JsonTextWriter(fileWriter))
                {
                    jsonWriter.WriteStartArray();
                    foreach (var collectionItem in listOfCollectionItems)
                    {
                        var itemToUpdate = collectionItem.ID.EqualsIgnoreCase(item.ID) ? item : collectionItem;
                        await jsonWriter.WriteValueAsync(itemToUpdate);
                    }
                    jsonWriter.WriteEndArray();
                }
                _fileWrapper.Delete(_filePath);
                _fileWrapper.Move(_tempFilePath, _filePath);
            }
            finally
            {
                _semaphore.Release();
            }
        }

        public async Task Upsert(T item)
        {
            ValidateCollectionItem(item);
            await _semaphore.WaitAsync();
            try
            {
                var listOfCollectionItems = GetCollection();
                using (FileStream fileStream = new FileStream(_tempFilePath, FileMode.Open, FileAccess.Write))
                using (StreamWriter fileWriter = new StreamWriter(fileStream))
                using (JsonTextWriter jsonWriter = new JsonTextWriter(fileWriter))
                {
                    var itemUpdated = false;
                    jsonWriter.WriteStartArray();
                    foreach (var collectionItem in listOfCollectionItems)
                    {
                        if (collectionItem.ID.EqualsIgnoreCase(item.ID))
                        {
                            await jsonWriter.WriteValueAsync(item);
                            itemUpdated = true;
                            continue;
                        }
                        await jsonWriter.WriteValueAsync(collectionItem);
                    }
                    if (!itemUpdated)
                        await jsonWriter.WriteValueAsync(item);
                    jsonWriter.WriteEndArray();
                }
                _fileWrapper.Delete(_filePath);
                _fileWrapper.Move(_tempFilePath, _filePath);
            }
            finally
            {
                _semaphore.Release();
            }
        }
    }
}
