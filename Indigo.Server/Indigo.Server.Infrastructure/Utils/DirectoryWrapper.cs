﻿using System.IO;
using Indigo.Server.Interfaces.Utils;

namespace Indigo.Server.Infrastructure.Utils
{
    public class DirectoryWrapper : IDirectoryWrapper
    {
        public bool Exists(string path)
        {
            return Directory.Exists(path);
        }

        public DirectoryInfo CreateDirectory(string path)
        {
           return Directory.CreateDirectory(path);
        }

        public DirectoryInfo CreateOrGetDirectory(string path)
        {
            if (Directory.Exists(path))
                return new DirectoryInfo(path);
            return Directory.CreateDirectory(path);
        }

        public void DeleteDirectory(string path)
        {
            Directory.Delete(path);
        }
    }
}