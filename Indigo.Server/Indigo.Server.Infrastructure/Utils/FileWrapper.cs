﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Indigo.Server.Interfaces.Utils;

namespace Indigo.Server.Infrastructure.Utils
{
    

    public class FileWrapper : IFileWrapper
    {
        public async Task<byte[]> ReadAllBytesAsync(string path, CancellationToken cancellationToken = default)
        {
            return await File.ReadAllBytesAsync(path, cancellationToken);
        }

        public async Task WriteAllBytesAsync(string path, byte[] bytes, CancellationToken cancellationToken = default)
        {
            await File.WriteAllBytesAsync(path, bytes, cancellationToken);
        }

        public async Task WriteAllTextAsync(string path, string text, CancellationToken cancellationToken = default)
        {
            var directoryName = Path.GetDirectoryName(path);
            if (!Directory.Exists(directoryName))
                Directory.CreateDirectory(directoryName);
            await File.WriteAllTextAsync(path, text, cancellationToken);
        }

        public async Task<string> ReadAllTextAsync(string path, CancellationToken cancellationToken = default)
        {
            var directoryName = Path.GetDirectoryName(path);
            if (!Directory.Exists(directoryName))
                Directory.CreateDirectory(directoryName);
            return await File.ReadAllTextAsync(path, cancellationToken);
        }

        public bool Exists(string path)
        {
            return File.Exists(path);
        }

        public void Delete(string path)
        {
            File.Delete(path);
        }
        public void Move(string sourcePath, string destPath)
        {
             File.Move(sourcePath, destPath);
        }

        public string ReadAllText(string path)
        {
            return File.ReadAllText(path);
        }
    }
}