﻿using System.Threading;
using System.Threading.Tasks;
using Indigo.Server.Interfaces.Infrastructure;
using Indigo.Server.Interfaces.Utils;

namespace Indigo.Server.Infrastructure
{
    public class FileSystemPackageSaver : IPackageSaver
    {
        private readonly IFileWrapper _file;
        private readonly string _rootPath;

        public FileSystemPackageSaver(IFileWrapper file, string rootPath)
        {
            _file = file;
            _rootPath = rootPath;
        }

        public async Task SaveFile(string packageName, string fileName, string fileContent, CancellationToken cancellationToken = default)
        {
            await _file.WriteAllTextAsync($"{_rootPath}/{packageName}/{fileName}", fileContent, cancellationToken);
        }

        public async Task SaveFile(string packageName, string fileName, byte[] fileContent, CancellationToken cancellationToken = default)
        {
            await _file.WriteAllBytesAsync($"{_rootPath}/{packageName}/{fileName}", fileContent, cancellationToken);
        }
    }
}