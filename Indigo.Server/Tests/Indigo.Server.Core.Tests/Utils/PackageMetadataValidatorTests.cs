using Indigo.Server.Core.Utils;
using Indigo.Server.Interfaces.Exceptions;
using Moq;
using NUnit.Framework;

namespace Indigo.Server.Core.Tests.Utils
{
    [TestFixture]
    public class PackageMetadataValidatorTests
    {
        private MockRepository mockRepository;



        [SetUp]
        public void SetUp()
        {
            mockRepository = new MockRepository(MockBehavior.Strict);
        }

        [TearDown]
        public void TearDown()
        {
            mockRepository.VerifyAll();
        }

        [Test]
        public void Validate_Should_Throw_Exception_If_Package_Names_Do_Not_Match()
        {
            // Arrange

            // Act
           var exception =  Assert.Throws< InvalidPackageMetadataException >( () => new PackageMetadataValidator().Validate("packageName", ("differentPackageName", "packageVersion", null)));

            // Assert
            Assert.AreEqual("Package name does not match with the metadata", exception.Message);
        }

        [Test]
        public void Validate_Should_Not_Throw_Exception_If_Package_Names_Match()
        {
            // Arrange

            // Act
            new PackageMetadataValidator().Validate("packageName", ("packageName", "packageVersion", null));

            // Assert - No Exception
        }

    }
}
