using Indigo.Server.Core.Utils;
using Indigo.Server.Utilities.Extensions;
using Moq;
using NUnit.Framework;

namespace Indigo.Server.Core.Tests.Utils
{
    [TestFixture]
    public class PackageMetadataMergerTests
    {
        private MockRepository mockRepository;

        [SetUp]
        public void SetUp()
        {
            mockRepository = new MockRepository(MockBehavior.Strict);
        }

        [TearDown]
        public void TearDown()
        {
            mockRepository.VerifyAll();
        }

        [Test]
        public void MergePackageMetadata_Should_Merge_New_Version_To_Existing_Metadata()
        {
            // Arrange
            var version1 = "version1: {dist : {tarball : 'tarball1.gz'}}";
            var version2 = "version2: {dist : {tarball : 'tarball2.gz'}}";


            var distTag1 = "tag1: 'tagvalue1'";
            var distTag2 = "tag2: 'tagvalue2'";


            var attachment1 = "fileName1: {version : 'version1' , shasum : '1234'}";
            var attachment2 = "fileName2: {version : 'version2', shasum : '5678'}";

            var currentMetadataString = $"{{ 'versions' : {{{version1}, {version2}}}, 'dist-tags': {{{distTag1}, {distTag2}}}, _attachments: {{{attachment1}, {attachment2}}}}}";

            var distTag3 = "tag3: 'tagvalue3'";

            var version3 = $"version3: {{version : 'version3', dist : {{tarball : 'tarball3.gz',  shasum : 'abcd'}}}}";

            var newMetadataString = $"{{  'versions' : {{{version3}}}, 'dist-tags': {{{distTag3}}}}}";

            var expectedMergedMetadata = $"{{ 'versions' : {{{version1}, {version2}, {version3}}}, 'dist-tags': {{{distTag1}, {distTag2}, {distTag3}}}, _attachments: {{{attachment1}, {attachment2},'tarball3.gz': {{version: 'version3', shasum : 'abcd'}}}}}}".ParseToPackageMetadata();

            // Act
            var mergedMetadata = new PackageMetadataMerger().MergePackageMetadata(currentMetadataString.ParseToPackageMetadata(), newMetadataString.ParseToPackageMetadata());

            // Assert

            Assert.AreEqual(expectedMergedMetadata, mergedMetadata);

        }

    }
}
