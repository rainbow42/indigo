using Indigo.Server.Core.Utls;
using Indigo.Server.Utilities.Extensions;
using Moq;
using NUnit.Framework;
using System;

namespace Indigo.Server.Core.Tests.Utils
{
    [TestFixture]
    public class PackageMetadataParserTests
    {
        private MockRepository mockRepository;



        [SetUp]
        public void SetUp()
        {
            mockRepository = new MockRepository(MockBehavior.Strict);
        }

        [TearDown]
        public void TearDown()
        {
            mockRepository.VerifyAll();
        }

        [Test]
        public void ParseMetadata_Should_Parse_With_Attachment()
        {
            // Arrange

            byte[] fileContent = new byte[] { 5, 6, 7, 8 };
            var metadata = ("{versions : {version1: {version : 'version1', name :'packageName', dist : {tarball : 'tarball2.gz'}}}, _attachments: {'version1.tgz': {data: '" + Convert.ToBase64String(fileContent) + "'}}}").ParseToPackageMetadata();

            // Act
            (string packageName, string packageVersion, byte[] tarball, string tarballFileName) = new PackageMetadataParser().ParseMetadata(metadata);


            // Assert
            Assert.AreEqual("packageName", packageName);
            Assert.AreEqual("version1", packageVersion);
            Assert.AreEqual(fileContent, tarball);
            Assert.AreEqual("version1.tgz", tarballFileName);

        }

        [Test]
        public void ParseMetadata_Should_Parse_Without_Attachment()
        {
            // Arrange

            var metadata = ("{versions : {version1: {version : 'version1', name :'packageName', dist : {tarball : 'tarball2.gz'}}}}").ParseToPackageMetadata();

            // Act
            (string packageName, string packageVersion, byte[] tarball, string tarballFileName) = new PackageMetadataParser().ParseMetadata(metadata);


            // Assert
            Assert.AreEqual("packageName", packageName);
            Assert.AreEqual("version1", packageVersion);
            Assert.IsNull(tarball);
            Assert.IsNull(tarballFileName);

        }

    }
}
