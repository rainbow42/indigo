using Indigo.Server.Core.Handlers;
using Indigo.Server.Models.Storage;
using Indigo.Server.Plugins.Storage;
using Indigo.Server.Utilities.Extensions;
using Moq;
using NUnit.Framework;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.Server.Core.Tests.Handlers
{
    [TestFixture]
    public class UnpublishPackageHandlerTests
    {
        private MockRepository mockRepository;

        private Mock<IBlobStore<PackageMetadataFile>> _metadataStore;
        private Mock<IBlobStore<PackageTarballFile>> _tarballStore;

        [SetUp]
        public void SetUp()
        {
            mockRepository = new MockRepository(MockBehavior.Strict);

            _metadataStore = this.mockRepository.Create<IBlobStore<PackageMetadataFile>>();
            _tarballStore = this.mockRepository.Create<IBlobStore<PackageTarballFile>>();
        }

        [TearDown]
        public void TearDown()
        {
            mockRepository.VerifyAll();
        }

        [Test]
        public async Task UnpublishPackage_Should_Delete_All_Versions_Of_A_Package()
        {
            // Arrange
            var packageName = "publishingPackageName";

            var version1 = "version1: {dist : {tarball : 'tarball1.gz'}}";
            var version2 = "version2: {dist : {tarball : 'tarball2.gz'}}";

            var currentMetadataString = $"{{ '_rev' : 'LatestRevision' , 'versions' : {{{version1}, {version2}}}}}";
            _metadataStore.Setup(m => m.GetFile($"{packageName}/package.json")).ReturnsAsync(Encoding.UTF8.GetBytes(currentMetadataString));


            _metadataStore.Setup(m => m.DeleteFile($"{packageName}/package.json")).Returns(Task.CompletedTask);
            _tarballStore.Setup(m => m.DeleteFile($"{packageName}/tarball1.gz")).Returns(Task.CompletedTask);
            _tarballStore.Setup(m => m.DeleteFile($"{packageName}/tarball2.gz")).Returns(Task.CompletedTask);
            // Act
            await new UnpublishPackageHandler(_metadataStore.Object, _tarballStore.Object).UnpublishPackage(packageName, null);


            // Assert - Verify all

        }


        [Test]
        public async Task UnpublishPackage_Should_Delete_Specific_FileName_Of_A_Package_Without_Deleting_Metadata()
        {
            // Arrange
            var packageName = "publishingPackageName";


            _tarballStore.Setup(m => m.DeleteFile($"{packageName}/tarball.gz")).Returns(Task.CompletedTask);
            // Act
            await new UnpublishPackageHandler(_metadataStore.Object, _tarballStore.Object).UnpublishPackage(packageName, "tarball.gz", null);


            // Assert - Verify all

        }


    }
}
