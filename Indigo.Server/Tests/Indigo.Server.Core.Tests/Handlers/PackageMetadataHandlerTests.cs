﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Indigo.Server.Core.Handlers;
using Indigo.Server.Interfaces.Exceptions;
using Indigo.Server.Interfaces.Infrastructure;
using Indigo.Server.Models.Storage;
using Indigo.Server.Plugins.Storage;
using Moq;
using NUnit.Framework;

namespace Indigo.Server.Core.Tests.Handlers
{
    [TestFixture]
    public class PackageMetadataHandlerTests
    {
        private Mock<IPackageMetadataProvider> _fileSystemProvider;
        private Mock<IPackageMetadataProvider> _httpProvider;
        private Mock<IBlobStore<PackageMetadataFile>> _metadatastore;
        private const string CurrentDomain = "http://localhost";
        private const string RemoteDomain = "http://registry.npmjs.com";

        [SetUp]
        public void Setup()
        {
            _httpProvider = new Mock<IPackageMetadataProvider>(MockBehavior.Strict);
            _fileSystemProvider = new Mock<IPackageMetadataProvider>(MockBehavior.Strict);
            _metadatastore = new Mock<IBlobStore<PackageMetadataFile>>(MockBehavior.Strict);
        }

        [TearDown]
        public void TearDown()
        {
            _fileSystemProvider.VerifyAll();
            _httpProvider.VerifyAll();
            _metadatastore.VerifyAll();
        }

        [Test]
        public async Task ShouldGetPackageMetadataFromRemoteIfDoesNotExistOnFileSystem()
        {
            //Arrange
            const string packgeName = "lodash";
            const string version = "lodash.2.1";
            const bool shouldGetPackageFromRemote = true;
            const string metaData = "{\"Name\": \"lodash\", \"Url\": \"http://registry.npmjs.com/lodash\"}";
            const string updatedMetaData = "{\"Name\": \"lodash\", \"Url\": \"http://localhost/lodash\"}";

            var handler = new PackageMetadataHandler(_fileSystemProvider.Object, _httpProvider.Object, _metadatastore.Object, shouldGetPackageFromRemote, CurrentDomain, RemoteDomain);

            _fileSystemProvider.Setup(s => s.GetPackageMetadata(packgeName, version)).ReturnsAsync((string)null);
            _httpProvider.Setup(s => s.GetPackageMetadata(packgeName, version)).ReturnsAsync(metaData);
            var collectionName = "CollectionName";
            _metadatastore.SetupGet(m => m.CollectionName).Returns(collectionName);
            _metadatastore.Setup(s => s.SaveFile($"{packgeName}/package.json", updatedMetaData, It.Is<PackageMetadataFile>(p => p.FileName == $"{packgeName}/package.json" && p.CollectionName == collectionName), false))
                .Returns(Task.FromResult("")); 

            var response = await handler.GetPackageMetadata(packgeName, version);

            Assert.That(response, Is.EqualTo(updatedMetaData));
        }

        [Test]
        public async Task ShouldGetPackageMetadataFromRemoteIfDoesNotExistOnFileSystemWhenVersionIsNull()
        {
            //Arrange
            const string packgeName = "lodash";
            const string version = null;
            const bool shouldGetPackageFromRemote = true;
            const string metaData = "{\"Name\": \"lodash\", \"Url\": \"http://registry.npmjs.com/lodash\"}";
            const string updatedMetaData = "{\"Name\": \"lodash\", \"Url\": \"http://localhost/lodash\"}";

            var handler = new PackageMetadataHandler(_fileSystemProvider.Object, _httpProvider.Object, _metadatastore.Object, shouldGetPackageFromRemote, CurrentDomain, RemoteDomain);

            _fileSystemProvider.Setup(s => s.GetPackageMetadata(packgeName, version)).ReturnsAsync((string)null);
            _httpProvider.Setup(s => s.GetPackageMetadata(packgeName, version)).ReturnsAsync(metaData);
            var collectionName = "CollectionName";
            _metadatastore.SetupGet(m => m.CollectionName).Returns(collectionName);
            _metadatastore.Setup(s => s.SaveFile($"{packgeName}/package.json", updatedMetaData, It.Is<PackageMetadataFile>(p => p.FileName == $"{packgeName}/package.json" && p.CollectionName == collectionName), false))
                .Returns(Task.FromResult(""));
            //_fileSystemProvider.Setup(s => s.GetPackageMetadata(packgeName, version)).ReturnsAsync(updatedMetaData);

            var response = await handler.GetPackageMetadata(packgeName, version);

            Assert.That(response, Is.EqualTo(updatedMetaData));
        }


        [Test]
        public async Task ShouldGetPackageMetadataFromFileSystemIfExists()
        {
            //Arrange
            const string packgeName = "lodash";
            const string version = "lodash.2.1";
            const bool shouldGetPackageFromRemote = true;
            const string metaData = "{\"Name\": \"lodash\", \"Url\": \"http://registry.npmjs.com/lodash\"}";

            var handler = new PackageMetadataHandler(_fileSystemProvider.Object, _httpProvider.Object, _metadatastore.Object, shouldGetPackageFromRemote, CurrentDomain, RemoteDomain);

            _fileSystemProvider.Setup(s => s.GetPackageMetadata(packgeName, version)).ReturnsAsync(metaData);
            //Act
            var response = await handler.GetPackageMetadata(packgeName, version);
            //Assert
            Assert.That(response, Is.EqualTo(metaData));
        }

        [Test]
        public async Task ShouldGetPackageMetadataFromFileSystemIfIffetchFromRemoteNotAllowed()
        {
            //Arrange
            const string packgeName = "lodash";
            const string version = "lodash.2.1";
            const bool shouldGetPackageFromRemote = false;
            const string metaData = "{\"Name\": \"lodash\", \"Url\": \"http://registry.npmjs.com/lodash\"}";

            var handler = new PackageMetadataHandler(_fileSystemProvider.Object, _httpProvider.Object, _metadatastore.Object, shouldGetPackageFromRemote, CurrentDomain, RemoteDomain);

            _fileSystemProvider.Setup(s => s.GetPackageMetadata(packgeName, version)).ReturnsAsync(metaData);
            //Act
            var response = await handler.GetPackageMetadata(packgeName, version);
            //Assert
            Assert.That(response, Is.EqualTo(metaData));
        }

        [Test]
        public void ShouldThrowExceptionIfIffetchFromRemoteNotAllowedAndPackageDoesNotExist()
        {
            //Arrange
            const string packgeName = "lodash";
            const string version = "lodash.2.1";
            const bool shouldGetPackageFromRemote = false;

            var handler = new PackageMetadataHandler(_fileSystemProvider.Object, _httpProvider.Object, _metadatastore.Object, shouldGetPackageFromRemote, CurrentDomain, RemoteDomain);

            _fileSystemProvider.Setup(s => s.GetPackageMetadata(packgeName, version)).ReturnsAsync((string)null);
            //Act, Assert

            var exception = Assert.ThrowsAsync<PackageNotFoundException>(() => handler.GetPackageMetadata(packgeName, version));
            //Assert

            Assert.That(exception.Message, Is.EqualTo("Package not found"));
        }

        [Test]
        public void ShouldThrowExceptionIfIffetchFromRemoteAllowedAndPackageDoesNotExist()
        {
            //Arrange
            const string packgeName = "lodash";
            const string version = "lodash.2.1";
            const bool shouldGetPackageFromRemote = true;
            const string metaData = "{\"Name\": \"lodash\", \"Url\": \"http://registry.npmjs.com/lodash\"}";
            const string updatedMetaData = "{\"Name\": \"lodash\", \"Url\": \"http://localhost/lodash\"}";

            var handler = new PackageMetadataHandler(_fileSystemProvider.Object, _httpProvider.Object, _metadatastore.Object, shouldGetPackageFromRemote, CurrentDomain, RemoteDomain);

            _fileSystemProvider.Setup(s => s.GetPackageMetadata(packgeName, version)).ReturnsAsync((string)null);
            _httpProvider.Setup(s => s.GetPackageMetadata(packgeName, version)).Throws(new HttpRequestException("404 (Not Found)"));
            //Act, Assert

            var exception = Assert.ThrowsAsync<PackageNotFoundException>(() => handler.GetPackageMetadata(packgeName, version));
            //Assert

            Assert.That(exception.Message, Is.EqualTo("Package not found"));
        }
    }
}