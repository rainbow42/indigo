﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Indigo.Server.Core.Handlers;
using Indigo.Server.Interfaces.Exceptions;
using Indigo.Server.Interfaces.Infrastructure;
using Indigo.Server.Models.Storage;
using Indigo.Server.Plugins.Storage;
using Moq;
using NUnit.Framework;

namespace Indigo.Server.Core.Tests.Handlers
{
    [TestFixture]
    public class PackageTarballHanderTests
    {
        private const string RemoteDomain = "registry.npmjs.org";
        private const string CurrentDomain = "localhost";
        private PackageTarballHander _handler;
        private Mock<ITarBallProvider> _fileSystemTarballProvider;
        private Mock<ITarBallProvider> _remoteTarballProvider;
        private Mock<IBlobStore<PackageTarballFile>> _tarballStore;
        [SetUp]
        public void Setup()
        {
            _fileSystemTarballProvider = new Mock<ITarBallProvider>(MockBehavior.Strict);
            _remoteTarballProvider = new Mock<ITarBallProvider>(MockBehavior.Strict);
            _tarballStore = new Mock<IBlobStore<PackageTarballFile>>(MockBehavior.Strict);
        }

        [TearDown]
        public void TearDown()
        {
            _fileSystemTarballProvider.VerifyAll();
            _remoteTarballProvider.VerifyAll();
            _tarballStore.VerifyAll();
        }

        [Test]
        public async Task ShouldGetTarBallFromFileSystemIfExists()
        {
            const bool getPackageFromRemote = true;
            const string packageName = "packageName";
            const string fileName = "fileName";
            var expectedResponse = new byte[] { 0, 5, 7 };
            _handler = new PackageTarballHander(_fileSystemTarballProvider.Object, _remoteTarballProvider.Object, _tarballStore.Object, getPackageFromRemote);

            _fileSystemTarballProvider.Setup(s => s.GetGZipBytes(packageName, fileName)).ReturnsAsync(expectedResponse);

            var actualResponse = await _handler.GetTarBall(packageName, fileName);

            Assert.That(actualResponse, Is.EqualTo(expectedResponse));
        }

        [Test]
        public async Task ShouldGetTarBallFromRemoteIfDoesNotExistInFileSystem()
        {
            const bool getPackageFromRemote = true;
            const string packageName = "packageName";
            const string fileName = "fileName";
            var expectedResponse = new byte[] { 0, 5, 7 };
            _handler = new PackageTarballHander(_fileSystemTarballProvider.Object, _remoteTarballProvider.Object, _tarballStore.Object, getPackageFromRemote);

            _fileSystemTarballProvider.Setup(s => s.GetGZipBytes(packageName, fileName)).ReturnsAsync((byte[])null);
            _remoteTarballProvider.Setup(s => s.GetGZipBytes(packageName, fileName)).ReturnsAsync(expectedResponse);
            var collectionName = "CollectionName";
            _tarballStore.SetupGet(m => m.CollectionName).Returns(collectionName);
            _tarballStore.Setup(s => s.SaveFile($"{packageName}/{fileName}", expectedResponse, It.Is<PackageTarballFile>(p => p.FileName == $"{packageName}/{fileName}" && p.CollectionName == collectionName), false))
                .Returns(Task.FromResult(""));


            var actualResponse = await _handler.GetTarBall(packageName, fileName);

            Assert.That(actualResponse, Is.EqualTo(expectedResponse));
        }

        [Test]
        public void ShouldThrowExceptionIfDoesNotExistInFileSystemAndNotAllowedToGetFromRemote()
        {
            const bool getPackageFromRemote = false;
            const string packageName = "packageName";
            const string fileName = "fileName";
            var expectedResponse = new byte[] { 0, 5, 7 };
            _handler = new PackageTarballHander(_fileSystemTarballProvider.Object, _remoteTarballProvider.Object, _tarballStore.Object, getPackageFromRemote);

            _fileSystemTarballProvider.Setup(s => s.GetGZipBytes(packageName, fileName)).ReturnsAsync((byte[])null);

            var ex = Assert.ThrowsAsync<PackageNotFoundException>(() => _handler.GetTarBall(packageName, fileName));

            Assert.That(ex.Message, Is.EqualTo("Package not found"));
        }

        [Test]
        public void ShouldThrowExceptionIfDoesNotExistInFileSystemAndAllowedToGetFromRemoteAndRemoteThrowsNotFound()
        {
            const bool getPackageFromRemote = true;
            const string packageName = "packageName";
            const string fileName = "fileName";
            var expectedResponse = new byte[] { 0, 5, 7 };
            _handler = new PackageTarballHander(_fileSystemTarballProvider.Object, _remoteTarballProvider.Object, _tarballStore.Object, getPackageFromRemote);

            _fileSystemTarballProvider.Setup(s => s.GetGZipBytes(packageName, fileName)).ReturnsAsync((byte[])null);
            _remoteTarballProvider.Setup(s => s.GetGZipBytes(packageName, fileName))
                .ThrowsAsync(new HttpRequestException("404 (Not Found)"));

            var ex = Assert.ThrowsAsync<PackageNotFoundException>(() => _handler.GetTarBall(packageName, fileName));

            Assert.That(ex.Message, Is.EqualTo("Package not found"));
        }
    }
}