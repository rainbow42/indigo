using Indigo.Server.Core.Handlers;
using Indigo.Server.Interfaces.Exceptions;
using Indigo.Server.Interfaces.Utils;
using Indigo.Server.Models;
using Indigo.Server.Models.Storage;
using Indigo.Server.Plugins.Storage;
using Indigo.Server.Utilities.Extensions;
using Moq;
using NUnit.Framework;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.Server.Core.Tests.Handlers
{
    [TestFixture]
    public class PublishPackageHandlerTests
    {
        private MockRepository mockRepository;

        private Mock<IBlobStore<PackageMetadataFile>> _metadataStore;
        private Mock<IBlobStore<PackageTarballFile>> _tarballStore;
        private Mock<IPackageMetadataMerger> _packageMetadataMerger;


        [SetUp]
        public void SetUp()
        {
            mockRepository = new MockRepository(MockBehavior.Strict);

            _metadataStore = this.mockRepository.Create<IBlobStore<PackageMetadataFile>>();
            _tarballStore = this.mockRepository.Create<IBlobStore<PackageTarballFile>>();
            _packageMetadataMerger = this.mockRepository.Create<IPackageMetadataMerger>();
        }

        [TearDown]
        public void TearDown()
        {
            mockRepository.VerifyAll();
        }

        [Test]
        public void PublishPackage_Should_Validate_IncomingPackge_If_Revision_Conflicts()
        {
            // Arrange
            var packageName = "publishingPackageName";

            var currentMetadataString = "{ '_rev' : 'LatestRevision' }";
            var newMetadataString = "{ '_rev' : 'LatestRevision' }";
            var newMetadata = newMetadataString.ParseToPackageMetadata();
            _metadataStore.Setup(m => m.IsFileExists($"{packageName}/package.json")).ReturnsAsync(true);
            _metadataStore.Setup(m => m.GetFile($"{packageName}/package.json")).ReturnsAsync(Encoding.UTF8.GetBytes(currentMetadataString));
            // Act & // Assert
            var publishPackageHandler = new PublishPackageHandler(_metadataStore.Object, _tarballStore.Object, _packageMetadataMerger.Object);
            byte[] fileContent = new byte[] { 1, 2, 3 };
            var exception = Assert.ThrowsAsync<InvalidPackageMetadataException>(() => publishPackageHandler.PublishPackage(packageName, "NewVersion", newMetadata, fileContent, "tarballFileName", "oldRevision"));
            Assert.AreEqual("Document update conflict.", exception.Message);
        }

        [Test]
        public void PublishPackage_Should_Validate_IncomingPackge_If_AlreadyExising_Package_And_no_Revision()
        {
            // Arrange
            var packageName = "publishingPackageName";

            var currentMetadataString = "{ '_rev' : 'LatestRevision' , 'versions': {'OldVersion1' : {}, 'OldVersion2' : {} }}";
            var newMetadataString = "{ '_rev' : 'LatestRevision' }";
            var newMetadata = newMetadataString.ParseToPackageMetadata();
            _metadataStore.Setup(m => m.IsFileExists($"{packageName}/package.json")).ReturnsAsync(true);
            _metadataStore.Setup(m => m.GetFile($"{packageName}/package.json")).ReturnsAsync(Encoding.UTF8.GetBytes(currentMetadataString));
            // Act & // Assert
            var publishPackageHandler = new PublishPackageHandler(_metadataStore.Object, _tarballStore.Object, _packageMetadataMerger.Object);
            byte[] fileContent = new byte[] { 1, 2, 3 };
            const string tarballFileName = "tarballFileName";
            var exception = Assert.ThrowsAsync<PackageAlreadyExistsException>(() => publishPackageHandler.PublishPackage(packageName, "OldVersion2", newMetadata, fileContent, tarballFileName));
            Assert.AreEqual("Cannot modify existing package version.", exception.Message);
        }


        [Test]
        public async Task PublishPackage_Should_Validate_IncomingPackge_If_AlreadyExising_Package_And_If_Latest_Revision_Present_Validation_Should_Pass()
        {
            // Arrange
            var packageName = "publishingPackageName";
            byte[] fileContent = new byte[] { 1, 2, 3 };
            var metadataCollectionName = "MetadataPrivateCollection";
            _metadataStore.Setup(m => m.CollectionName).Returns(metadataCollectionName);


            var tarballCollectionName = "TarballPrivateCollection";
            _tarballStore.Setup(m => m.CollectionName).Returns(tarballCollectionName);

            var currentMetadataString = "{ '_rev' : 'LatestRevision' , 'versions': {'OldVersion1' : {}, 'OldVersion2' : {} }}";
            var newMetadataString = "{ '_rev' : 'LatestRevision' }";
            var newMetadata = newMetadataString.ParseToPackageMetadata();
            const string tarballFileName = "tarballFileName";
            _metadataStore.Setup(m => m.IsFileExists($"{packageName}/package.json")).ReturnsAsync(true);
            _metadataStore.Setup(m => m.GetFile($"{packageName}/package.json")).ReturnsAsync(Encoding.UTF8.GetBytes(currentMetadataString));
            _metadataStore.Setup(m => m.SaveFile($"{packageName}/package.json",
                                                                    It.Is<string>(str => str == newMetadata.ToString()),
                                                                    It.Is<PackageMetadataFile>(p => p.CollectionName == metadataCollectionName && p.FileName == $"{packageName}/package.json"),
                                                                    false)).Returns(Task.CompletedTask);

            _tarballStore.Setup(m => m.SaveFile($"{packageName}/{tarballFileName}",
                                                        fileContent,
                                                        It.Is<PackageTarballFile>(p => p.CollectionName == tarballCollectionName && p.FileName == $"{packageName}/{tarballFileName}"),
                                                        false)).Returns(Task.CompletedTask);

            // Act 
            var publishPackageHandler = new PublishPackageHandler(_metadataStore.Object, _tarballStore.Object, _packageMetadataMerger.Object);

            await publishPackageHandler.PublishPackage(packageName, "OldVersion2", newMetadata, fileContent, tarballFileName, "LatestRevision");


            // Assert - Verify All
        }


        [Test]
        public async Task PublishPackage_Should_Validate_IncomingPackge_If_NotAlreadyExising_Package_Should_Save_Package()
        {
            // Arrange
            var packageName = "publishingPackageName";
            byte[] fileContent = new byte[] { 1, 2, 3 };
            var metadataCollectionName = "MetadataPrivateCollection";
            _metadataStore.Setup(m => m.CollectionName).Returns(metadataCollectionName);


            var tarballCollectionName = "TarballPrivateCollection";
            _tarballStore.Setup(m => m.CollectionName).Returns(tarballCollectionName);

            var newMetadataString = "{ '_rev' : 'LatestRevision' }";
            var newMetadata = newMetadataString.ParseToPackageMetadata();
            var mergedMetadata = "{}".ParseToPackageMetadata();
            _packageMetadataMerger.Setup(pm => pm.MergePackageMetadata(null, newMetadata)).Returns(mergedMetadata);

            const string tarballFileName = "tarballFileName";
            _metadataStore.Setup(m => m.IsFileExists($"{packageName}/package.json")).ReturnsAsync(false);
            _metadataStore.Setup(m => m.SaveFile($"{packageName}/package.json",
                                                                    It.Is<string>(str => str == mergedMetadata.ToString()),
                                                                    It.Is<PackageMetadataFile>(p => p.CollectionName == metadataCollectionName && p.FileName == $"{packageName}/package.json"),
                                                                    false)).Returns(Task.CompletedTask);

            _tarballStore.Setup(m => m.SaveFile($"{packageName}/{tarballFileName}",
                                                        fileContent,
                                                        It.Is<PackageTarballFile>(p => p.CollectionName == tarballCollectionName && p.FileName == $"{packageName}/{tarballFileName}"),
                                                        false)).Returns(Task.CompletedTask);

            // Act 
            var publishPackageHandler = new PublishPackageHandler(_metadataStore.Object, _tarballStore.Object, _packageMetadataMerger.Object);

            await publishPackageHandler.PublishPackage(packageName, "NewVersion", newMetadata, fileContent, tarballFileName);


            // Assert - Verify All
        }

        [Test]
        public async Task PublishPackage_Should_Should_Save_Merged_package()
        {
            // Arrange
            var packageName = "publishingPackageName";
            byte[] fileContent = new byte[] { 1, 2, 3 };
            var metadataCollectionName = "MetadataPrivateCollection";
            _metadataStore.Setup(m => m.CollectionName).Returns(metadataCollectionName);


            var tarballCollectionName = "TarballPrivateCollection";
            _tarballStore.Setup(m => m.CollectionName).Returns(tarballCollectionName);

            var currentMetadataString = "{ '_rev' : 'LatestRevision' , 'versions': {'OldVersion1' : {}, 'OldVersion2' : {} }}";
            var newMetadataString = "{ '_rev' : 'LatestRevision' }";
            var newMetadata = newMetadataString.ParseToPackageMetadata();

            var mergedMetadata = "{}".ParseToPackageMetadata();
            _packageMetadataMerger.Setup(pm => pm.MergePackageMetadata(It.Is<PackageMetadata>(str => str.ToString() == currentMetadataString.ParseToPackageMetadata().ToString()), newMetadata)).Returns(mergedMetadata);

            const string tarballFileName = "tarballFileName";
            _metadataStore.Setup(m => m.IsFileExists($"{packageName}/package.json")).ReturnsAsync(true);
            _metadataStore.Setup(m => m.GetFile($"{packageName}/package.json")).ReturnsAsync(Encoding.UTF8.GetBytes(currentMetadataString));
            _metadataStore.Setup(m => m.SaveFile($"{packageName}/package.json",
                                                                    It.Is<string>(str => str == mergedMetadata.ToString()),
                                                                    It.Is<PackageMetadataFile>(p => p.CollectionName == metadataCollectionName && p.FileName == $"{packageName}/package.json"),
                                                                    false)).Returns(Task.CompletedTask);

            _tarballStore.Setup(m => m.SaveFile($"{packageName}/{tarballFileName}",
                                                        fileContent,
                                                        It.Is<PackageTarballFile>(p => p.CollectionName == tarballCollectionName && p.FileName == $"{packageName}/{tarballFileName}"),
                                                        false)).Returns(Task.CompletedTask);

            // Act 
            var publishPackageHandler = new PublishPackageHandler(_metadataStore.Object, _tarballStore.Object, _packageMetadataMerger.Object);

            await publishPackageHandler.PublishPackage(packageName, "NewVersion", newMetadata, fileContent, tarballFileName);


            // Assert - Verify All
        }


        [Test]
        public async Task PublishPackage_Should_Save_Only_Metadata_If_No_Tarball()
        {
            // Arrange
            var packageName = "publishingPackageName";
            var metadataCollectionName = "MetadataPrivateCollection";
            _metadataStore.Setup(m => m.CollectionName).Returns(metadataCollectionName);



            var newMetadataString = "{ '_rev' : 'LatestRevision' }";
            var newMetadata = newMetadataString.ParseToPackageMetadata();
            var mergedMetadata = "{}".ParseToPackageMetadata();
            _packageMetadataMerger.Setup(pm => pm.MergePackageMetadata(null, newMetadata)).Returns(mergedMetadata);

            const string tarballFileName = "tarballFileName";
            _metadataStore.Setup(m => m.IsFileExists($"{packageName}/package.json")).ReturnsAsync(false);
            _metadataStore.Setup(m => m.SaveFile($"{packageName}/package.json",
                                                                    It.Is<string>(str => str == mergedMetadata.ToString()),
                                                                    It.Is<PackageMetadataFile>(p => p.CollectionName == metadataCollectionName && p.FileName == $"{packageName}/package.json"),
                                                                    false)).Returns(Task.CompletedTask);

          
            // Act 
            var publishPackageHandler = new PublishPackageHandler(_metadataStore.Object, _tarballStore.Object, _packageMetadataMerger.Object);

            await publishPackageHandler.PublishPackage(packageName, "NewVersion", newMetadata, null, tarballFileName);


            // Assert - Verify All
        }


    }
}
