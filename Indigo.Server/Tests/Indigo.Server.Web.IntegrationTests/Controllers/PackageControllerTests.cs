﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;

namespace Indigo.Server.Web.IntegrationTests.Controllers
{
    [TestFixture]
    public class PackageControllerTests
    {
        private readonly HttpClient _client;
        private readonly TestServer _server;

        public PackageControllerTests()
        {
            var jsonFile = @"C:\MyWorks[Temp]\indigo\Indigo.Server\Tests\Indigo.Server.Web.IntegrationTests\appsettings.json";
            var webHost = new WebHostBuilder().UseConfiguration(new ConfigurationBuilder().AddJsonFile(jsonFile).Build()).UseStartup<Startup>();
            _server = new TestServer(webHost);
            _client = _server.CreateClient();
        }

        [Test]
        public async Task ShouldGetPackageResponse()
        {
            var response = await _client.GetAsync("/lodash");
            var responseContent = await response.Content.ReadAsStringAsync();
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            //Assert.That(responseContent, Is.EqualTo("the input package was lodash"));
        }

        [Test]
        public async Task ShouldGetPackageAndVersionResponse()
        {
            var response = await _client.GetAsync("/lodash/4.17.0");
            var responseContent = await response.Content.ReadAsStringAsync();
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            //Assert.That(responseContent, Is.EqualTo("the input package was lodash@2.1.1"));
        }

        [Test]
        public async Task ShouldGetTarBallWhenAskedWithVersion()
        {
            var response = await _client.GetAsync("lodash/-/lodash-4.17.0.tgz");
            var responseContent = response.Content.ReadAsByteArrayAsync();
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        [Test]
        public async Task ShouldGetTarBallWhenAskedWithoutVersion()
        {
            var timer = Stopwatch.StartNew();
            var response = await _client.GetAsync("lodash/-/lodash-4.17.5.tgz");
            timer.Stop();
            var ellapsed = timer.ElapsedMilliseconds;
            var responseContent = response.Content.ReadAsByteArrayAsync();
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }
    }
}