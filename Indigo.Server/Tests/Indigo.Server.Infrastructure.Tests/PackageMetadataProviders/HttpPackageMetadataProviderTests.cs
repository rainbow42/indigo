﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Indigo.Server.Infrastructure.PackageMetadataProviders;
using Indigo.Server.Infrastructure.Tests.Utils;
using NUnit.Framework;

namespace Indigo.Server.Infrastructure.Tests.PackageMetadataProviders
{
    [TestFixture]
    public class HttpPackageMetadataProviderTests
    {
        private const string FakeAddress = "http://www.fakeAddress.com";

        private HttpPackageMetadataProvider _provider;
        private FakeResponseHandler _fakeResponseHandler;
        private HttpClient _httpClient;
        [SetUp]
        public void Setup()
        {
            _fakeResponseHandler = new FakeResponseHandler();
            _httpClient = new HttpClient(_fakeResponseHandler) { BaseAddress = new Uri(FakeAddress) };
            _provider = new HttpPackageMetadataProvider(_httpClient);
        }

        [TearDown]
        public void TearDown()
        {
            _httpClient.Dispose();
            _fakeResponseHandler = null;
            _provider = null;
        }

        [Test]
        public async Task ShouldGetPackageMetadataFromNpmIfVersionExists()
        {
            const string packageName = "lodash";
            const string version = "2.1.1";
            const string responseString = "{\"Name\": \"Lodash\"}";
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent(responseString) };
            _fakeResponseHandler.AddFakeResponse(new Uri($"{FakeAddress}/{packageName}/{version}"), httpResponseMessage);

            var actualResponse = await _provider.GetPackageMetadata(packageName, version);

            Assert.That(actualResponse, Is.EqualTo(responseString));
        }

        [Test]
        public async Task ShouldGetPackageMetadataFromNpmIfVersionDoesNotExists()
        {
            const string packageName = "lodash";
            const string responseString = "{\"Name\": \"Lodash\"}";
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent(responseString) };
            _fakeResponseHandler.AddFakeResponse(new Uri($"{FakeAddress}/{packageName}"), httpResponseMessage);

            var actualResponse = await _provider.GetPackageMetadata(packageName, null);

            Assert.That(actualResponse, Is.EqualTo(responseString));
        }

        [Test]
        public void ShouldGetExceptionIfNpmReturnsIt()
        {
            const string packageName = "lodash";
            const string responseString = "{\"Name\": \"Lodash\"}";
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.InternalServerError) { Content = new StringContent(responseString) };
            _fakeResponseHandler.AddFakeResponse(new Uri($"{FakeAddress}/{packageName}"), httpResponseMessage);

            Assert.ThrowsAsync<HttpRequestException>(async () => await _provider.GetPackageMetadata(packageName, null));
        }


    }
}