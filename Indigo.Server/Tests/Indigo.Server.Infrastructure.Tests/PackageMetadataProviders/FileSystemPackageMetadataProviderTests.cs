using Indigo.Server.Infrastructure.PackageMetadataProviders;
using Indigo.Server.Models.Storage;
using Indigo.Server.Plugins.Storage;
using Moq;
using NUnit.Framework;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.Server.Infrastructure.Tests.PackageMetadataProviders
{
    [TestFixture]
    public class FileSystemPackageMetadataProviderTests
    {
        private MockRepository mockRepository;

        private Mock<IBlobStore<PackageMetadataFile>> _privateMetadataStore;
        private Mock<IBlobStore<PackageMetadataFile>> _cacheMetadataStore;

        [SetUp]
        public void SetUp()
        {
            mockRepository = new MockRepository(MockBehavior.Strict);

            _privateMetadataStore = this.mockRepository.Create<IBlobStore<PackageMetadataFile>>();
            _cacheMetadataStore = this.mockRepository.Create<IBlobStore<PackageMetadataFile>>();
        }

        [TearDown]
        public void TearDown()
        {
            mockRepository.VerifyAll();
        }

        [Test]
        public async Task GetPackageMetadata_Should_GetMetadataFrom_PrivateStore()
        {
            // Arrange
            var packageName = "packageName";
            var packageVersion = "packageVersion";
            _privateMetadataStore.Setup(pm => pm.IsFileExists($"{packageName}/package.json")).ReturnsAsync(true);
            _privateMetadataStore.Setup(pm => pm.GetFile($"{packageName}/package.json")).ReturnsAsync(Encoding.UTF8.GetBytes("PackageMetaData"));

            // Act
            var provider = new FileSystemPackageMetadataProvider(_privateMetadataStore.Object, _cacheMetadataStore.Object);
            var result = await provider.GetPackageMetadata(packageName, packageVersion);

            // Assert
            Assert.AreEqual("PackageMetaData", result);

        }

        [Test]
        public async Task GetPackageMetadata_Should_GetMetadataFrom_CacheStore()
        {
            // Arrange
            var packageName = "packageName";
            var packageVersion = "packageVersion";
            _privateMetadataStore.Setup(pm => pm.IsFileExists($"{packageName}/package.json")).ReturnsAsync(false);
            _cacheMetadataStore.Setup(pm => pm.IsFileExists($"{packageName}/package.json")).ReturnsAsync(true);
            _cacheMetadataStore.Setup(pm => pm.GetFile($"{packageName}/package.json")).ReturnsAsync(Encoding.UTF8.GetBytes("PackageMetaData"));

            // Act
            FileSystemPackageMetadataProvider provider = new FileSystemPackageMetadataProvider(_privateMetadataStore.Object, _cacheMetadataStore.Object);
            var result = await provider.GetPackageMetadata(packageName, packageVersion);

            // Assert
            Assert.AreEqual("PackageMetaData", result);

        }

        [Test]
        public async Task GetPackageMetadata_Should_Return_Null_If_Not_Exists_In_Private_Or_Cache()
        {
            // Arrange
            var packageName = "packageName";
            var packageVersion = "packageVersion";
            _privateMetadataStore.Setup(pm => pm.IsFileExists($"{packageName}/package.json")).ReturnsAsync(false);
            _cacheMetadataStore.Setup(pm => pm.IsFileExists($"{packageName}/package.json")).ReturnsAsync(false);

            // Act
            FileSystemPackageMetadataProvider provider = new FileSystemPackageMetadataProvider(_privateMetadataStore.Object, _cacheMetadataStore.Object);
            var result = await provider.GetPackageMetadata(packageName, packageVersion);

            // Assert
            Assert.IsNull(result);

        }


    }
}
