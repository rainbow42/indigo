using Indigo.Server.Infrastructure.TarBallProviders;
using Indigo.Server.Models.Storage;
using Indigo.Server.Plugins.Storage;
using Moq;
using NUnit.Framework;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.Server.Infrastructure.Tests.TarBallProviders
{
    [TestFixture]
    public class FileSystemTarBallProviderTests
    {
        private MockRepository mockRepository;

        private Mock<IBlobStore<PackageTarballFile>> _privateTarballStore;
        private Mock<IBlobStore<PackageTarballFile>> _cachedTarballStore;

        [SetUp]
        public void SetUp()
        {
            mockRepository = new MockRepository(MockBehavior.Strict);

            _privateTarballStore = this.mockRepository.Create<IBlobStore<PackageTarballFile>>();
            _cachedTarballStore = this.mockRepository.Create<IBlobStore<PackageTarballFile>>();
        }

        [TearDown]
        public void TearDown()
        {
            mockRepository.VerifyAll();
        }

        [Test]
        public async Task GetGZipBytes_Should_get_Tarbal_From_PrivateStore()
        {
            // Arrange
            var packageName = "packageName";
            var fileName = "fileName.tgz";
            _privateTarballStore.Setup(pm => pm.IsFileExists($"{packageName}/{fileName}")).ReturnsAsync(true);
            var fileContent = new byte[] { 1, 2, 3 };
            _privateTarballStore.Setup(pm => pm.GetFile($"{packageName}/{fileName}")).ReturnsAsync(fileContent);

            // Act
            var provider = new FileSystemTarBallProvider(_privateTarballStore.Object, _cachedTarballStore.Object);
            var result = await provider.GetGZipBytes(packageName, fileName);

            // Assert
            Assert.AreEqual(fileContent, result);
        }


        [Test]
        public async Task GetGZipBytes_Should_get_Tarbal_From_CacheStore_If_Not_Resnent_In_Private_Store()
        {
            // Arrange
            var packageName = "packageName";
            var fileName = "fileName.tgz";
            _privateTarballStore.Setup(pm => pm.IsFileExists($"{packageName}/{fileName}")).ReturnsAsync(false);
            _cachedTarballStore.Setup(pm => pm.IsFileExists($"{packageName}/{fileName}")).ReturnsAsync(true);
            var fileContent = new byte[] { 1, 2, 3 };
            _cachedTarballStore.Setup(pm => pm.GetFile($"{packageName}/{fileName}")).ReturnsAsync(fileContent);

            // Act
            FileSystemTarBallProvider provider = new FileSystemTarBallProvider(_privateTarballStore.Object, _cachedTarballStore.Object);
            var result = await provider.GetGZipBytes(packageName, fileName);

            // Assert
            Assert.AreEqual(fileContent, result);
        }

        [Test]
        public async Task GetGZipBytes_Should_return_null_if_not_present()
        {
            // Arrange
            var packageName = "packageName";
            var fileName = "fileName.tgz";
            _privateTarballStore.Setup(pm => pm.IsFileExists($"{packageName}/{fileName}")).ReturnsAsync(false);
            _cachedTarballStore.Setup(pm => pm.IsFileExists($"{packageName}/{fileName}")).ReturnsAsync(false);

            // Act
            FileSystemTarBallProvider provider = new FileSystemTarBallProvider(_privateTarballStore.Object, _cachedTarballStore.Object);
            var result = await provider.GetGZipBytes(packageName, fileName);

            // Assert
            Assert.IsNull(result);
        }


    }
}
