﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Indigo.Server.Infrastructure.TarBallProviders;
using Indigo.Server.Infrastructure.Tests.Utils;
using Moq;
using Moq.Protected;
using NUnit.Framework;

namespace Indigo.Server.Infrastructure.Tests.TarBallProviders
{
    [TestFixture]
    public class HttpTarBallProviderTests
    {
        private const string FakeAddress = "http://www.fakeAddress.com";
        private HttpTarBallProvider _tarBallProvider;
        private FakeResponseHandler _mockHttpMessageHandler;
        private HttpClient _httpClient;
        [SetUp]
        public void Setup()
        {
            _mockHttpMessageHandler = new FakeResponseHandler();
            _httpClient = new HttpClient(_mockHttpMessageHandler) {BaseAddress = new Uri(FakeAddress)};
            _tarBallProvider = new HttpTarBallProvider(_httpClient);

        }

        [TearDown]
        public void TearDown()
        {
            _httpClient.Dispose();
        }

        [Test]
        public async Task ShouldGetTarballFromRemoteRepository()
        {
            //Arrange
            const string packageName = "lodash";
            const string fileName = "lodash-2.1.txt";
            var uri = $"{packageName}/-/{fileName}";
            var expectedContent = new byte[] {1, 0, 2, 5, 8, 40};
            _mockHttpMessageHandler.AddFakeResponse(new Uri($"{FakeAddress}/{uri}"), new HttpResponseMessage(HttpStatusCode.OK){Content = new ByteArrayContent(expectedContent)} );
            //Act
            var actualResponse = await _tarBallProvider.GetGZipBytes(packageName, fileName);
            //Assert
            Assert.That(actualResponse, Is.EqualTo(expectedContent));

        }

        [Test]
        public void ShouldNotGetTarballFromRemoteRepositoryIfCallFails()
        {
            //Arrange
            const string packageName = "lodash";
            const string fileName = "lodash-2.1.txt";
            var uri = $"{packageName}/-/{fileName}";
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            _mockHttpMessageHandler.AddFakeResponse(new Uri($"{FakeAddress}/{uri}"), httpResponseMessage );
            //Act & Assert
            Assert.ThrowsAsync<HttpRequestException>(async () => await _tarBallProvider.GetGZipBytes(packageName, fileName));
        }
    }
}