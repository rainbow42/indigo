﻿using System.Threading;
using System.Threading.Tasks;
using Indigo.Server.Interfaces.Utils;
using Moq;
using NUnit.Framework;

namespace Indigo.Server.Infrastructure.Tests
{
    [TestFixture]
    public class FileSystemPackageSaverTests
    {
        private FileSystemPackageSaver _fileSystemPackageSaver;
        private Mock<IFileWrapper> _mockFile;
        private const string RootPath = "RootPath";

        [SetUp]
        public void Setup()
        {
            _mockFile = new Mock<IFileWrapper>(MockBehavior.Strict);
            _fileSystemPackageSaver = new FileSystemPackageSaver(_mockFile.Object, RootPath);
        }

        [TearDown]
        public void TearDown()
        {
            _mockFile.VerifyAll();
        }

        [Test]
        public async Task ShouldSavePackageTextFile()
        {
            const string packageName = "packageName";
            const string fileName = "fileName";
            const string fileContent = "fileContent";
            var filePath = $"{RootPath}/{packageName}/{fileName}";
            _mockFile.Setup(s => s.WriteAllTextAsync(filePath, fileContent, CancellationToken.None)).Returns(Task.FromResult(""));

            await _fileSystemPackageSaver.SaveFile(packageName, fileName, fileContent,CancellationToken.None);
        }
        [Test]
        public async Task ShouldSavePackageBinaryFile()
        {
            const string packageName = "packageName";
            const string fileName = "fileName";
            byte[] fileContent = {1, 5, 1, 8, 7};
            var filePath = $"{RootPath}/{packageName}/{fileName}";
            _mockFile.Setup(s => s.WriteAllBytesAsync(filePath, fileContent, CancellationToken.None)).Returns(Task.FromResult(""));
            await _fileSystemPackageSaver.SaveFile(packageName, fileName, fileContent, CancellationToken.None);

        }

    }
}