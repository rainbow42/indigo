﻿using Indigo.Server.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.Server.Interfaces.Handlers
{
    public interface IPublishPackageHandler
    {
        Task PublishPackage(string packageName, string packageVersion, PackageMetadata metadata, byte[] fileContent, string tarballFileName, string latestRevision = null);
    }
}
