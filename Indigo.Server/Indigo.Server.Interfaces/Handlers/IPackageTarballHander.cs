﻿using System.Threading;
using System.Threading.Tasks;

namespace Indigo.Server.Interfaces.Handlers
{
    public interface IPackageTarballHander
    {
        Task<byte[]> GetTarBall(string packageName, string fileName, CancellationToken cancellationToken = default);
    }
}