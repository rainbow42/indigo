﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.Server.Interfaces.Handlers
{
    public interface IUnpublishPackageHandler
    {
        Task UnpublishPackage(string packageName, string fileName, string revision);
        Task UnpublishPackage(string packageName, string revision);
    }
}
