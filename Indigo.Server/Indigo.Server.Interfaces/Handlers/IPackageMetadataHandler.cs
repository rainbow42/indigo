﻿using System.Threading;
using System.Threading.Tasks;

namespace Indigo.Server.Interfaces.Handlers
{
    public interface IPackageMetadataHandler
    {
        Task<string> GetPackageMetadata(string packageName, string version, CancellationToken cancellationToken = default);
    }
}