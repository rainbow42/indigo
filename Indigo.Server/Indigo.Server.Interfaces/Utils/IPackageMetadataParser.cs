﻿using Indigo.Server.Models;

namespace Indigo.Server.Interfaces.Utils
{
    public interface IPackageMetadataParser
    {
        (string packageName, string packageVersion, byte[] tarball, string tarballFileName) ParseMetadata(PackageMetadata metadata);
    }
}