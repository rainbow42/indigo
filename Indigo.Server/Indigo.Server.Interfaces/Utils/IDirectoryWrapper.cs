﻿using System.IO;

namespace Indigo.Server.Interfaces.Utils
{
   public interface IDirectoryWrapper
    {
        bool Exists(string path);
        DirectoryInfo CreateDirectory(string path);
        DirectoryInfo CreateOrGetDirectory(string path);
        void DeleteDirectory(string path);
    }
}