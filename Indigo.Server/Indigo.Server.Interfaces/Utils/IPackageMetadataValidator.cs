﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Indigo.Server.Interfaces.Utils
{
    public interface IPackageMetadataValidator
    {
        void Validate(string packageName, (string packageName, string packageVersion, byte[] tarball) parsedPackageDetails);
    }
}
