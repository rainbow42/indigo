﻿using Indigo.Server.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Indigo.Server.Interfaces.Utils
{
    public interface IPackageMetadataMerger
    {
        PackageMetadata MergePackageMetadata(PackageMetadata current, PackageMetadata newVersion);
    }
}
