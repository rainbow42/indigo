﻿using System.Threading;
using System.Threading.Tasks;

namespace Indigo.Server.Interfaces.Utils
{
    public interface IFileWrapper
    {
        Task<byte[]> ReadAllBytesAsync(string path, CancellationToken cancellationToken = default);
        Task<string> ReadAllTextAsync(string path, CancellationToken cancellationToken = default);
        Task WriteAllBytesAsync(string path, byte[] bytes, CancellationToken cancellationToken = default);
        Task WriteAllTextAsync(string path, string text, CancellationToken cancellationToken = default);

        string ReadAllText(string path);

        bool Exists(string path);
        void Delete(string path);
        void Move(string sourcePath, string destPath);
    }
}