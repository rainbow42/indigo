﻿using System.Threading;
using System.Threading.Tasks;

namespace Indigo.Server.Interfaces.Infrastructure
{
    public interface IPackageSaver
    {
        Task SaveFile(string packageName, string fileName, string fileContent, CancellationToken cancellationToken = default);
        Task SaveFile(string packageName, string fileName, byte[] fileContent, CancellationToken cancellationToken = default);
    }
}