﻿using System.Threading.Tasks;

namespace Indigo.Server.Interfaces.Infrastructure
{
    public interface ITarBallProvider
    {
        Task<byte[]> GetGZipBytes(string packageName, string fileName);
    }
}