﻿using System.Threading.Tasks;

namespace Indigo.Server.Interfaces.Infrastructure
{
    public interface IPackageMetadataProvider
    {
        Task<string> GetPackageMetadata(string package, string version);
    }
}