﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Indigo.Server.Interfaces.Exceptions
{
    public class PackageAlreadyExistsException : Exception
    {
        public PackageAlreadyExistsException() : base() { }
        public PackageAlreadyExistsException(string message) : base(message) { }
    }
}
