﻿using System;

namespace Indigo.Server.Interfaces.Exceptions
{
    public class PackageNotFoundException : Exception
    {
        public PackageNotFoundException(string message) : base(message)
        {
            
        }
    }
}