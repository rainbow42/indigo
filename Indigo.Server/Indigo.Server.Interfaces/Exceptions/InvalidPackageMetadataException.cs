﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Indigo.Server.Interfaces.Exceptions
{
    public class InvalidPackageMetadataException: Exception
    {
        public InvalidPackageMetadataException() : base() { }
        public InvalidPackageMetadataException(string message) : base(message) { }
    }
}
