﻿using Indigo.Server.Plugins.Storage.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.Server.Plugins.Storage
{
    public interface ICollectionStore<T> where T : ICollectionItem
    {
        string CollectionName { get; }

        Task Insert(T item);
        Task Update(T item);
        Task Upsert(T item);

        Task Delete(T item);
        Task Delete(string id);

        Task<T> GetOne(string id);
        Task<T> GetOne(Func<T, bool> criteria);

        Task<IEnumerable<T>> Get(Func<T, bool> criteria);
    }
}
