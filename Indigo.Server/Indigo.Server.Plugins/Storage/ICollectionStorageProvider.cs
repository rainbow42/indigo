﻿using Indigo.Server.Plugins.Storage.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.Server.Plugins.Storage
{
    public interface ICollectionStorageProvider
    {
        Task<ICollectionStore<T>> CreateOrGetExistingStore<T>(string collectionName) where T : ICollectionItem;
        Task DeleteStore<T>(string collectionName) where T : ICollectionItem;
    }
}
