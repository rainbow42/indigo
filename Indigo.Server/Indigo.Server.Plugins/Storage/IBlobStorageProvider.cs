﻿using Indigo.Server.Plugins.Storage.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.Server.Plugins.Storage
{
    public interface IBlobStorageProvider
    {
        Task<IBlobStore<T>> CreateOrGetExistingStore<T>(string collectionName) where T : IBlobMetadata;
        Task DeleteStore<T>(string collectionName) where T : IBlobMetadata;
    }
}
