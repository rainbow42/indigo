﻿using Indigo.Server.Plugins.Storage.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Indigo.Server.Plugins.Storage
{
    public interface IBlobStore<T> where T : IBlobMetadata
    {
        string CollectionName { get; }

        Task SaveFile(string fileName, byte[] data, T metadata, bool overwrite = false);
        Task SaveFile(string fileName, string data, T metadata, bool overwrite = false);
        Task<byte[]> GetFile(string fileName);
        Task DeleteFile(string fileName);
        Task<T> GetMetadata(string fileName);
        Task UpdateMetadata(string fileName, T metadata);
        Task UpdateFileContent(string fileName, byte[] data);
        Task UpdateFileContent(string fileName, string data);
        Task<bool> IsFileExists(string fileName);
        Task<IEnumerable<T>> SearchFiles(Func<T, bool> criteriaForMetadata);
        Task<IEnumerable<string>> SearchFiles(Func<string, bool> criteriaForFileName);
    }
}
