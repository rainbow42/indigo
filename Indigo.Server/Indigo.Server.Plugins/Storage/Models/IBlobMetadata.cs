﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Indigo.Server.Plugins.Storage.Models
{
    public interface IBlobMetadata
    {
        string CollectionName { get; set; }
        string FileName { get; set; }
    }
}
