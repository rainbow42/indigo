﻿using Indigo.Server.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Indigo.Server.Utilities.Extensions
{
    public static class StringX
    {
        public static bool EqualsIgnoreCase(this string thisString, string otherString)
        {
            return string.Equals(thisString, otherString, StringComparison.OrdinalIgnoreCase);
        }

        public static bool IsNullOrEmpty(this string thisString)
        {
            return string.IsNullOrEmpty(thisString);
        }

        public static bool IsNullOrWhiteSpace(this string thisString)
        {
            return string.IsNullOrWhiteSpace(thisString);
        }

        public static PackageMetadata ParseToPackageMetadata(this string thisString)
        {
            return PackageMetadata.Parse(thisString) ;
        }
    }
}
