﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Indigo.Server.Utilities.Extensions
{
    public static class ObjectX
    {
        public static bool IsNull(this object thisObject)
        {
            return thisObject == null;
        }

        public static bool IsNotNull(this object thisObject)
        {
            return thisObject != null;
        }
    }
}
